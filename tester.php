<?php
require_once root . 'DB/DatabaseManager.php';
require_once root . 'Display/Display.php';
require_once root . 'Router.php';

class Message {

    private $message_id;
    private $reseller_id;
    private $phone_number;
    private $sender_id;
    private $message;
    private $page_length;
    private $telecom_id;
    private $smsc_response;
    private $smsc_id;
    private $porta_response;
    private $dlr_sent;
    private $date_sent;
    private $date_processed;
    private $mclass;
    private $sub;
    private $dlvrd;
    private $submit_date;
    private $done_date;
    private $report_url;
    private $other_parameter;
    private $error;
    private $request_id;
    private $number_to_send;
    private $money;

    public function __construct($reseller_id) {

        if ($reseller_id != "") {
            $this->setMessage ( $_GET ['message'] );
            $this->setSender_id ( $_GET ['sender'] );
            $this->setPhone_number ( urldecode($_GET ['recipient']) );

            $this->setDate_processed ( $_GET ['delivery'] );
            if ($_GET ['flash'] == 1) {
                $this->setMclass ( 0 );
            } else {
                $this->setMclass ( 1 );
            }
            $this->setOther_parameter ( $_GET ['otherparameter'] );
            $this->setReport_url ( $_GET ['url'] );
            $this->setReseller_id ( $reseller_id );
            $this->setPage_length ( $this->getMessagelength ( $_GET ['message'] ) );
            $this->number_to_send = array ();

            $db = new DatabaseManager ();
            $db->doQuery ( "INSERT INTO request (reseller_id, recipients,sender_id, message, date_request,flash)
		VALUES ('" . $this->getReseller_id () . "','" . $this->getPhone_number () . "','" . addslashes($this->getSender_id ()) . "',
		'" . addslashes($this->getMessage ()) . "','" . time () . "','" . $this->getMclass () . "')" );
            $id = $db->getInsertID ();
            $this->setRequest_id ( $id );
            $db->close ();
        } else {
            //mail ( "masteryomi@gmail.com", "I got to message", "Cla initialized" );
        }

    }



    function process_All() {
        /*
         * The numbers expected are int he format +234
         * I am expecting numbers seperated by comma
         */

        $router = new Router ( $_GET ['recipient'], $this->getReseller_id () );
        $response = $router->doAll ();
        if ($response == 0) {
            //get Message length and multiply it with the price


            $total_price = ( float ) ($this->getPage_length () * $router->getPrice ());
            $this->setMoney ( $total_price );
            $response = $total_price;
        }

        $db = new DatabaseManager ();
        $unknown = implode ( ",", $router->getUnknown () );
        $misplaced = implode ( ",", $router->getMisplaced () );
        $valid = implode ( ",", $router->getValid_numbers () );
        $invalid = implode ( ",", $router->getInvalid () );
        $duplicates = implode ( ",", $router->getDuplicate () );
        $db->doQuery ( "INSERT INTO processing (unknown, invalid,valid,misplaced,
					duplicates,request_id,date_processed) VALUES
					('$unknown','$invalid','$valid','$misplaced','$duplicates'
					,'" . $this->getRequest_id () . "','" . time () . "')" );

        $this->setNumber_to_send ( $router->getValid_numbers () );
        $db->close ();
        return $response;

    }

    function prepareMessage() {
        $db = new DatabaseManager ();
        foreach ( $this->getNumber_to_send () as $valid ) {
            $db->doQuery ( "INSERT INTO message
			(phone_number, request_id, reseller_id,date_sent,sender_id, mclass,report_url,other_parameter,smsc_response,remarks,page_length)
			 VALUES ('" . $valid . "','" . $this->getRequest_id () . "','" . $this->getReseller_id () . "',
			 '" . time () . "','" . addslashes ( $this->getSender_id () ) . "','" . $this->getMclass () . "',
			 '" . $this->getReport_url () . "','" . $this->getOther_parameter () . "','SENT','Delivered to Network','" . $this->getPage_length () . "')" );
        }
        $db->close ();

    }

    function prepareNumber() {
        $this->setPhone_number ( implode ( ",", $this->getNumber_to_send () ) );
    }
    public function formatMessage() {
        $message = $_GET ['message'];
        $message = mb_convert_encoding ( $message, "7bit", "UTF-8" );
        $this->setMessage ( $message );
        return $message;
    }

    public function getMessagelength($message) {
        $message = $this->formatMessage ();
        if (strlen ( $message ) <= 160) {
            //$this->setPage_length(1);
            return 1;
        } else {
            $one = strlen ( $message ) - 160;
            $two = ceil ( $one / 153 );
            $three = 1 + $two;
            //$this->setPage_length($three);
            return $three;
        }
    }

    function process_report_other() {
        $db = new DatabaseManager ();
        $db->doQuery ( "SELECT * FROM message WHERE  sent_status = 2 limit 100" );
        if($db->countRS() > 0){
            $arr = array();$arr2 = array();
            while($row = $db->getRS ()){
                if ($row ['report_url'] != "") {
                    $remark = $this->determineRemark ( $row['smsc_response']);
                    $request_id = $row['request_id'];
                    $url = $row ['report_url'] ."?message_id=".$row['message_id']."&status=".$row['smsc_response']."&remarks=".urlencode($remark)."&recipient=".urlencode($row['phone_number'])."&date_processed=".urlencode(time())."&requestid=".$request_id."&otherparameter=".$row['other_parameter'];
                    print_r($row); echo "<br>";
                    $answer = file_get_contents($url);

                    if($row['smsc_response'] =='DELIVERED' or $row['smsc_response'] =='UNDELIVERED' or $row['smsc_response'] =='REJECTED'){
                        array_push($arr,$row['message_id']);
                        echo "Treated transaction for ". $row['phone_number']. "-{$row['sender_id']}- {$row['smsc_response']}<br>";
                    }
                }//end if
            }//end while
            //$db->close ();
            if(count($arr) > 0) {
                $s = implode(",", $arr);
                $ss = trim($s);
                echo $ss;
                //$db = new DatabaseManager ();
                $db->doQuery("UPDATE message SET sent_status = 1 WHERE message_id in ($ss)");
                //$db->close();}
            }

            $db->close();
        }
        else{ echo "Nothing to treat";}


    }

    function process_report() {
        $db = new DatabaseManager ();
        $db->doQuery ( "SELECT * FROM message WHERE  report_url !='' and sent_status = 0 limit 100" );
        if($db->countRS() > 0){
            $arr = array();$arr2 = array();
            while($row = $db->getRS ()){
                if ($row ['report_url'] != "") {
                    $remark = $this->determineRemark ( $row['smsc_response']);
                    $request_id = $row['request_id'];
                    $url = $row ['report_url'] ."?message_id=".$row['message_id']."&status=".$row['smsc_response']."&remarks=".urlencode($remark)."&recipient=".urlencode($row['phone_number'])."&date_processed=".urlencode(time())."&requestid=".$request_id."&otherparameter=".$row['other_parameter'];
                    print_r($row); echo "<br>";
                    $answer = file_get_contents($url);

                    if($row['smsc_response'] =='DELIVERED' or $row['smsc_response'] =='UNDELIVERED' or $row['smsc_response'] =='REJECTED'){
                        array_push($arr,$row['message_id']);
                        echo "Treated transaction for ". $row['phone_number']. "-{$row['sender_id']}- {$row['smsc_response']}<br>";
                    }else{array_push($arr2,$row['message_id']);}
                }//end if
            }//end while
            //$db->close ();
            if(count($arr) > 0) {
                $s = implode(",", $arr);
                $ss = trim($s);
                echo $ss;
                //$db = new DatabaseManager ();
                $db->doQuery("UPDATE message SET sent_status = 1 WHERE message_id in ($ss)");
                //$db->close();}
            }
                if (count($arr2) > 0) {
                    $s = implode(",", $arr);
                    $ss = trim($s);
                    echo $ss;
                    //$db = new DatabaseManager ();
                    $db->doQuery("UPDATE message SET sent_status = 2 WHERE message_id in ($ss)");
                    //}

                }//end if
            $db->close();
            }
                else{ echo "Nothing to treat";}


            }
            function determineRemark($status) {
                switch ($status) {
                    case "QUEUED" :
                        return "Waiting for delivery report";
                        break;
                    case "REQUEUED" :
                        return "Waiting for delivery report";
                        break;
                    case "SUBMITTED" :
                        return "Message queued by network to be delivered later";
                        break;
                    case "DELIVERED" :
                        return "Message delivered to subscriber";
                        break;
                    case "UNDELIVERED" :
                        return "Message NOT delivered to subscriber. Call number to confirm";
                        break;
                    case "REJECTED" :
                        return "System malfunction of Network";
                        break;
                    case "":
                        return "Message received";
                        break;
                    default :
                        return "Subscriber not available";
                        break;
                }
            }

            function getTelecomId($receiver) {
                $db = new DatabaseManager ();
                $gsm = substr ( $receiver, 0, 7 );
                $cdma = substr ( $receiver, 0, 8 );
                $db->doQuery ( "select telecom_id as id from prefixs where prefix='$cdma' or prefix='$gsm'" );
                $row = $db->getRS ();
                $db->close ();
                return $row ['id'];
            }
            /**
             * @return the $message_id
             */
            public function getMessage_id() {
                return $this->message_id;
            }

            /**
             * @return the $reseller_id
             */
            public function getReseller_id() {
                return $this->reseller_id;
            }

            /**
             * @return the $phone_number
             */
            public function getPhone_number() {
                return $this->phone_number;
            }

            /**
             * @return the $sender_id
             */
            public function getSender_id() {
                return $this->sender_id;
            }

            /**
             * @return the $message
             */
            public function getMessage() {
                return $this->message;
            }

            /**
             * @return the $page_length
             */
            public function getPage_length() {
                return $this->page_length;
            }

            /**
             * @return the $telecom_id
             */
            public function getTelecom_id() {
                return $this->telecom_id;
            }

            /**
             * @return the $smsc_response
             */
            public function getSmsc_response() {
                return $this->smsc_response;
            }

            /**
             * @return the $smsc_id
             */
            public function getSmsc_id() {
                return $this->smsc_id;
            }

            /**
             * @return the $porta_response
             */
            public function getPorta_response() {
                return $this->porta_response;
            }

            /**
             * @return the $dlr_sent
             */
            public function getDlr_sent() {
                return $this->dlr_sent;
            }

            /**
             * @return the $date_sent
             */
            public function getDate_sent() {
                return $this->date_sent;
            }

            /**
             * @return the $date_processed
             */
            public function getDate_processed() {
                return $this->date_processed;
            }

            /**
             * @return the $mclass
             */
            public function getMclass() {
                return $this->mclass;
            }

            /**
             * @return the $sub
             */
            public function getSub() {
                return $this->sub;
            }

            /**
             * @return the $dlvrd
             */
            public function getDlvrd() {
                return $this->dlvrd;
            }

            /**
             * @return the $submit_date
             */
            public function getSubmit_date() {
                return $this->submit_date;
            }

            /**
             * @return the $done_date
             */
            public function getDone_date() {
                return $this->done_date;
            }

            /**
             * @return the $report_url
             */
            public function getReport_url() {
                return $this->report_url;
            }

            /**
             * @return the $other_parameter
             */
            public function getOther_parameter() {
                return $this->other_parameter;
            }

            /**
             * @return the $error
             */
            public function getError() {
                return $this->error;
            }

            /**
             * @return the $request_id
             */
            public function getRequest_id() {
                return $this->request_id;
            }

            /**
             * @param field_type $message_id
             */
            public function setMessage_id($message_id) {
                $this->message_id = $message_id;
            }

            /**
             * @param field_type $reseller_id
             */
            public function setReseller_id($reseller_id) {
                $this->reseller_id = $reseller_id;
            }

            /**
             * @param field_type $phone_number
             */
            public function setPhone_number($phone_number) {
                $this->phone_number = $phone_number;
            }

            /**
             * @param field_type $sender_id
             */
            public function setSender_id($sender_id) {
                $this->sender_id = $sender_id;
            }

            /**
             * @param field_type $message
             */
            public function setMessage($message) {
                $this->message = $message;
            }

            /**
             * @param field_type $page_length
             */
            public function setPage_length($page_length) {
                $this->page_length = $page_length;
            }

            /**
             * @param field_type $telecom_id
             */
            public function setTelecom_id($telecom_id) {
                $this->telecom_id = $telecom_id;
            }

            /**
             * @param field_type $smsc_response
             */
            public function setSmsc_response($smsc_response) {
                $this->smsc_response = $smsc_response;
            }

            /**
             * @param field_type $smsc_id
             */
            public function setSmsc_id($smsc_id) {
                $this->smsc_id = $smsc_id;
            }

            /**
             * @param field_type $porta_response
             */
            public function setPorta_response($porta_response) {
                $this->porta_response = $porta_response;
            }

            /**
             * @param field_type $dlr_sent
             */
            public function setDlr_sent($dlr_sent) {
                $this->dlr_sent = $dlr_sent;
            }

            /**
             * @param field_type $date_sent
             */
            public function setDate_sent($date_sent) {
                $this->date_sent = $date_sent;
            }

            /**
             * @param field_type $date_processed
             */
            public function setDate_processed($date_processed) {
                $this->date_processed = $date_processed;
            }

            /**
             * @param field_type $mclass
             */
            public function setMclass($mclass) {
                $this->mclass = $mclass;
            }

            /**
             * @param field_type $sub
             */
            public function setSub($sub) {
                $this->sub = $sub;
            }

            /**
             * @param field_type $dlvrd
             */
            public function setDlvrd($dlvrd) {
                $this->dlvrd = $dlvrd;
            }

            /**
             * @param field_type $submit_date
             */
            public function setSubmit_date($submit_date) {
                $this->submit_date = $submit_date;
            }

            /**
             * @param field_type $done_date
             */
            public function setDone_date($done_date) {
                $this->done_date = $done_date;
            }

            /**
             * @param field_type $report_url
             */
            public function setReport_url($report_url) {
                $this->report_url = $report_url;
            }

            /**
             * @param field_type $other_parameter
             */
            public function setOther_parameter($other_parameter) {
                $this->other_parameter = $other_parameter;
            }

            /**
             * @param field_type $error
             */
            public function setError($error) {
                $this->error = $error;
            }

            /**
             * @param field_type $request_id
             */
            public function setRequest_id($request_id) {
                $this->request_id = $request_id;
            }
            /**
             * @return the $number_to_send
             */
            public function getNumber_to_send() {
                return $this->number_to_send;
            }

            /**
             * @param field_type $number_to_send
             */
            public function setNumber_to_send($number_to_send) {
                $this->number_to_send = $number_to_send;
            }
            /**
             * @return the $money
             */
            public function getMoney() {
                return $this->money;
            }

            /**
             * @param field_type $money
             */
            public function setMoney($money) {
                $this->money = $money;
            }

        }

        ?>