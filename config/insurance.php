<?php
use Illuminate\Support\Facades\Config;


return [

    'app_name' => 'Online Insurance',
    'app_contact' => 'Online Insurance Tech',
    'company_logo_path' => 'public/assets/logo/company/',
    'policy_logo_path' => 'public/assets/logo/policy/',
    'document_path' => 'public/assets/documents/',
    'insurance_company' => '2',
    'default_company' => 3,
    'default_role_id' => 4,
    'mobileno'=> '+234 803 352 0140',
    'default_seller' => 'Online Insurance',
    'url' => 'https://ibank.gtbank.com/GTPay/Tranx.aspx',
    'url1' => 'https://ibank.gtbank.com',
    'url2' => '/GTPay/Tranx.aspx',
    'merchant_id'=>'678',//678,
    'return_url' => 'http://www.onlineinsuranceng.com/user/complete',
    'minimum'=> 1000,
    'email_subject' => 'Welcome to Online Insurance: Buy Any Insurance Policy',
    'email_from' => 'no-reply@onlineinsuranceng.com',
    'email_subject_activation' => 'Your Online Insurance Account Was Activated Succesfully',
    'currency' => 566,
    'gtpay_requery_url'=> 'https://ibank.gtbank.com/GTPayService/gettransactionstatus.json?',
    'gtpay_hashkey' => 'DBCEC683F039DDF86402F5BC450D6EF1CF308029695307ED863BD27873D9B7DDB1C7390DE0BC9BAC5CD96098AA2BDA0E8022ED241D33DC6D5CF96346C58D8C09',
    'certificate_status' => 10,
    'default_merchant_id'=> 5,
    'default_merchant_percentage' => 0.5




];