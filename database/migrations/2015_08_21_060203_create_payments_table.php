<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments',function(Blueprint $table){
        $table->increments('payment_id');
            $table->timestamps();
            $table->integer('user_id');
            $table->softDeletes();
            $table->string('payment_token');
            $table->integer('status');
            $table->integer('company_insurance_scheme_type_id');
            $table->decimal('sum_insured');
            $table->decimal('total_amount');
            $table->decimal('transaction_charge');
            $table->decimal('approved_amount');
            $table->string('switch_status');
            $table->string('switch_reference');
            $table->string('switch_Description');
            $table->timestamp('switch_return_time');
            $table->integer('treated_by');
            $table->boolean('remind_me');




    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
