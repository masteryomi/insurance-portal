<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings',function(Blueprint $table){

            $table->increments('setting_id');
            $table->boolean('customer_bears_transaction_charge');
            $table->boolean('customer_receives_sms_after_payment');
            $table->boolean('customer_receives_email_after_payment');
            $table->boolean('company_receives_sms_after_payment');
            $table->boolean('company_receives_email_after_payment');
            $table->boolean('admin_receives_email_after_payment');
            $table->boolean('admin_receives_sms_after_payment');
            $table->boolean('customer_email_validated');
            $table->boolean('customer_phone_validated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
