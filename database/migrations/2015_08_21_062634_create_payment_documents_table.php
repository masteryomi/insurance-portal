<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_documents',function(Blueprint $table){
        $table->increments('payment_document_id');
        $table->integer('document_id');
        $table->integer('payment_id');
        $table->string('document_name');
        $table->string('document_path');
        $table->timestamp('date_uploaded');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_documents');
    }
}
