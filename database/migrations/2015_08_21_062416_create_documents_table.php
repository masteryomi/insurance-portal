<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents',function(Blueprint $table){
        $table->increments('document_id');
        $table->integer('company_insurance_scheme_type_id');
        $table->string('document_name');
        $table->string('document_short_name');
        $table->string('document_description');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
