<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInsuranceSchemeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_insurance_scheme_types',function(Blueprint $table){
            $table->increments('company_insurance_scheme_type_id');
            $table->integer('company_insurance_scheme_id');
            $table->string('insurance_scheme_type_description');
            $table->string('scheme_type_short_name');
            $table->string('scheme_type_logo');
            $table->decimal('scheme_type_percentage');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('status');
            $table->boolean('is_minimum_sum_insured_set');
            $table->decimal('minimum_sum_insured')->nullable();
            $table->boolean('is_scheme_type_created_by_admin');
            $table->integer('created_by');
            $table->integer('approved_by')->nullable();
            $table->boolean('is_document_upload_required');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_insurance_scheme_types');
    }
}
