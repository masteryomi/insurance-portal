<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies',function(Blueprint $table ){

            $table->increments('company_id');
            $table->string('company_name');
            $table->string('company_address');
            $table->timestamps();
            $table->softDeletes();
            $table->string('contact_person');
            $table->string('contact_person_mobile');
            $table->string('contact_person_email');
            $table->string('company_logo');
            $table->integer('status');
            $table->integer('company_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
