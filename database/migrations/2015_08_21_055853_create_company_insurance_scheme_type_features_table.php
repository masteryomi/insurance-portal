<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInsuranceSchemeTypeFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_insurance_scheme_type_features',function(Blueprint $table){
            $table->increments('company_insurance_scheme_type_feature_id');
            $table->integer('company_insurance_scheme_type_id');
            $table->string('features');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('status');
            $table->boolean('is_scheme_type_created_by_admin');
            $table->integer('created_by');
            $table->integer('approved_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_insurance_scheme_type_features');
    }
}
