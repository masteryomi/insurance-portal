<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_policies', function (Blueprint $table) {
        $table->increments('id');
            $table->softDeletes();
        $table->integer('category_id')->references('id')->on('categories');
        $table->string('name')->unique();
        $table->string('description');
        $table->string('logo');
        $table->timestamps();
    });

        Schema::create('offerings', function (Blueprint $table) {
        $table->increments('id');
            $table->softDeletes();
        $table->integer('insurance_policy_id')->references('id')->on('insurance_policies');
        $table->integer('company_id')->references('company_id')->on('companies');
        $table->string('name');
        $table->string('description');
        $table->decimal('percentage');
        $table->decimal('percentage_off');
        $table->timestamps();
        $table->index(['company_id', 'name','insurance_policy_id']);
    });

        Schema::create('offering_features', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->integer('offerings_id')->references('id')->on('offerings');
            $table->string('description');
            $table->timestamps();
            $table->index(['offerings_id', 'description']);
        });
        Schema::create('payment_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->string('name')->unique();

        });

        Schema::create('user_offerings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->softDeletes();
            $table->integer('offerings_id')->references('id')->on('offerings');
            $table->integer('user_id')->references('user_id')->on('users');
            $table->timestamp('date_requested');
            $table->timestamps();
            $table->decimal('sum_insured');
            $table->decimal('rebate');
            $table->integer('status_id')->references('id')->on('payment_statuses');
            $table->bigInteger("reference_no");
            $table->decimal("surcharge");
            $table->decimal("total_amount");
            $table->decimal("approved_amount");
            $table->timestamp("response_from_switch");
            $table->string("response_code");
            $table->string("response_description");

        });

        Schema::create('document_types', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->string('name')->unique();

        });

        Schema::create('user_offerings_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->integer('user_offerings_id')->references('id')->on('user_offerings');
            $table->integer('document_type_id')->references('id')->on('document_types');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurance_policies');
        Schema::drop('offerings');
        Schema::drop('user_offerings');
        Schema::drop('payment_statuses');
        Schema::drop('document_types');
        Schema::drop('user_offerings_documents');
        Schema::drop('offering_features');
    }
}
