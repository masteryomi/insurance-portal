<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInsuranceSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_insurance_schemes',function(Blueprint $table){
            $table->increments('company_insurance_scheme_id');
            $table->integer('insurance_scheme_id');
            $table->integer('company_id');
            $table->string('insurance_scheme_description');
            $table->string('scheme_short_name');
            $table->string('scheme_logo');
            $table->text('scheme_type_features');
            $table->decimal('scheme_type_percentage');
            $table->boolean('is_minimum_sum_insured_set');
            $table->decimal('minimum_sum_insured')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->integer('status');
            $table->boolean('is_scheme_type_created_by_admin');
            $table->integer('created_by');
            $table->integer('approved_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_insurance_schemes');
    }
}
