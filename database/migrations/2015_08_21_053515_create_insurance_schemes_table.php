<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_schemes',function(Blueprint $table){
           $table->increments('insurance_scheme_id');
            $table->string('scheme_name');
            $table->string('scheme_description');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('status');
            $table->boolean('is_scheme_created_by_admin');
            $table->integer('created_by');
            $table->integer('approved_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurance_schemes');
    }
}
