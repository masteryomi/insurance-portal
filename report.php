<?php


class Report {



    public function __construct() {

    }

    function transmitDlr()
    {
        //now connect to soa and pick the messages
        $soa = mysql_connect("localhost","mytxt_report","p@55w0rd@001!");
        mysql_select_db("mytxt_soa",$soa);

        $query = mysql_query("select * from snd where dlr_id = > 0 and final_status > 0  and dlr_push_status = 0 limit 1000"
            ,$soa) or die (mysql_error());

        if(mysql_num_rows($query) == 0) {
            @mysql_free_result($query);
            @mysql_close($soa);
            return "No data to process at the moment";
        }

        $queryConnect = @mysql_connect("localhost","mytxt_query","p@55w0rd@001!");
        @mysql_select_db("mytxt_resellers",$queryConnect);
        $arr = array();
        while($row = mysql_fetch_array($query)) {

            $doit = mysql_query("update message set smsc_response = '".$row['smsc_response']."'
             where phone_number = '{$row['snd_to']}' and request_id = '{$row['message_id']}' and reseller_id = '{$row['reseller_id']}'"
                ,$queryConnect) or die (mysql_error());
            if(mysql_affected_rows($queryConnect)> 0)
            {
                array_push($arr,$row['snd_id']);
            }
        }
        @mysql_free_result($query);
        @mysql_close($queryConnect);

        $splice = implode(",",$arr);

        $query = mysql_query("update snd set dlr_push_status = 1 where snd_id in ($splice)"
            ,$soa) or die (mysql_error());

        echo mysql_affected_rows($soa)." rows were updated";
        @mysql_free_result($query);
        @mysql_close($soa);

    }

    function process()
    {
        if(!isset($_GET['username'])) return -1; //no username set
        if(!isset($_GET['password'])) return -2 ;// no password
        if($_GET["mid"]<=0) return -3 ;// invalid message id

        $queryConnect = @mysql_connect("localhost","mytxt_query","p@55w0rd@001!");
        @mysql_select_db("mytxt_resellers",$queryConnect);

        $user = mysql_query("select reseller_id from reseller_settings where
          username = '".mysql_real_escape_string($_GET['username'])."' and
                  password = '".md5(mysql_real_escape_string($_GET['password']))."'",$queryConnect);
        if(mysql_num_rows($user) == 0)
        {
            @mysql_free_result($user);
            @mysql_close($queryConnect);
            return -4; //invalid username or passsword
        }
        else {
            $row = mysql_fetch_array($user);
            $rid = $row["reseller_id"];
            @mysql_free_result($user);
            @mysql_close($queryConnect);

            //now connect to soa and pick the messages
            $soa = mysql_connect("localhost","mytxt_report","p@55w0rd@001!");
            mysql_select_db("mytxt_soa",$soa);

            $query = mysql_query("select snd_id ,snd_to,snd_sentat,snd_success,snd_failure,
            snd_submitted,snd_rejected,snd_buffered,snd_intermediate from snd where
            reseller_id = '".mysql_real_escape_string($rid)."' AND
            message_id = '".mysql_real_escape_string($_GET['mid'])."'",$soa) or die (mysql_error());

            if(mysql_num_rows($query) == 0) {
                @mysql_free_result($query);
                @mysql_close($soa);
                return - 5; //no such data exist
            }

            $emparray[] = array();
            while($row = mysql_fetch_array($query)) {
                $msg = new MessageDto();
                $msg->setMessageId($row["snd_id"]);
                $msg->setRecipient($row["snd_to"]);
                $msg->setDateSent($row["snd_sentat"]);

                //determine status
                if(isset($row["snd_success"])) {
                    $msg->setStatus("DELIVERED");
                    $msg->setDate($row["snd_success"]);
                    $emparray [] = (array)$msg;
                    continue;
                }
                if(isset($row["snd_failure"])) {
                    $msg->setStatus("FAILED");
                    $msg->setDate($row["snd_failure"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }

                if(isset($row["snd_submitted"])) {
                    $msg->setStatus("SUBMITTED");
                    $msg->setDate($row["snd_submitted"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }

                if(isset($row["snd_buffered"])) {
                    $msg->setStatus("BUFFERED");
                    $msg->setDate($row["snd_buffered"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }
                if(isset($row["snd_rejected"])) {
                    $msg->setStatus("REJECTED");
                    $msg->setDate($row["snd_rejected"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }

                if(isset($row["snd_intermediate"])) {
                    $msg->setStatus("INTERMEDIATE");
                    $msg->setDate($row["snd_intermediate"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }else
                {
                    $msg->setStatus("AWAITING_DLR");
                    //$msg->setDate($row["snd_intermediate"]);
                    $emparray [] =  (array)$msg;
                    continue;
                }

            }
            mysql_free_result($query);
            mysql_close($soa);

            return json_encode($emparray);

        }
    }

}

class MessageDto{

    public $messageId;
    public $status;
    public $date;
    public $recipient;
    public $dateSent;

    /**
     * @return mixed
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * @param mixed $dateSent
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * @param mixed $messageId
     */
    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;
    }

}


$report = new Report();
echo $report->transmitDlr();

?>