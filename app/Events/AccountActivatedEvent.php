<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
class AccountActivatedEvent extends Event
{
    use SerializesModels;

    public $user;

    public function __construct($input)
    {
        $this->user = $input;
    }
}
