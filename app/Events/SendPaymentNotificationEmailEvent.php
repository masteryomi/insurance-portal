<?php

namespace App\Events;

use App\Events\Event;
use App\UserOffering;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPaymentNotificationEmailEvent extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $userOffering;
    public function __construct(UserOffering $useroffering)
    {
        $this->userOffering = $useroffering;
    }


}
