<?php

namespace App\Service;
use  App\User;
class UserService
{

    public $user;
    public function all()
    {
        return User::paginate(10);
    }

    public function updateUser()
    {
        $result = array();
        $fname = \Input::get('first_name');
        $last_name = \Input::get('last_name');
        $middle_name = \Input::get('middle_name');
        $mobile_no = \Input::get('mobile_no');
        $address =   \Input::get('address');
        $city =  \Input::get('city');

        $user = \Auth::user();

        $mobileExist = User::where('mobile_number','=',$mobile_no)->first();
        if($mobileExist !=null)
        {
            if( $mobileExist->email != $user->email)
            {
                $result['status'] = false;
                $result['message'] = "The mobile number is already in use by another user";
                return $result;
            }
        }



        $user->city = $city;
        $user->address = $address;
        $user->first_name = $fname;
        $user->middle_name = $middle_name;
        $user->last_name = $last_name;
        $user->mobile_number= $mobile_no;
        $user->updated_at = date('Y-m-d H:i:s',time());

         if( $user->save())
         {
             $result['status'] = true;
             $result['message'] = "";
             return $result;
         }
        $result['status'] = false;
        $result['message'] = "Error occured. Please try again";
        return $result;

    }

    public function addUser()
    {
        $result = array();
        $fname = \Input::get('first_name');
        $last_name = \Input::get('last_name');
        $middle_name = \Input::get('middle_name');
        $mobile_no = \Input::get('mobile_no');
        $role_id =  config("insurance.default_role_id");
        $email = \Input::get('email');
        $password = \Input::get('password');


        $user = new User();
        $user->first_name = $fname;
        $user->last_name = $last_name;
        $user->middle_name = $middle_name;
        $user->role_id = $role_id;
        $user->status = 1;
        $user->mobile_number = $mobile_no;
        $user->email = $email;
        $user->password = \Hash::make($password);
        $user->created_at = date('Y-m-d H:i:s',time());
        $user->updated_at = date('Y-m-d H:i:s',time());
        $user->company_id = config('insurance.default_company');
        $user->confirmation_token = str_random(60).time();


        if($user->save())
        {
            $request['status'] = true;
            $request['message'] = 'User was successfully created. An email has been sent to you. PLease check it to continue';
            $request["user"] = $user;
        }
        else
        {
            $request['status'] = false;
            $request['message'] = 'User was not created. An error occured';
        }

        return $request;

    }


    public function addUserMerchant()
    {
        $result = array();
        $fname = \Input::get('first_name');
        $last_name = \Input::get('last_name');
        $middle_name = \Input::get('middle_name');
        $mobile_no = \Input::get('mobile_no');
        $role_id =  config("insurance.default_merchant_id");
        $email = \Input::get('email');
        $password = \Input::get('password');


        $user = new User();
        $user->first_name = $fname;
        $user->last_name = $last_name;
        $user->middle_name = $middle_name;
        $user->role_id = $role_id;
        $user->status = 1;
        $user->mobile_number = $mobile_no;
        $user->email = $email;
        $user->password = \Hash::make($password);
        $user->created_at = date('Y-m-d H:i:s',time());
        $user->updated_at = date('Y-m-d H:i:s',time());
        $user->company_id = config('insurance.default_company');
        $user->confirmation_token = str_random(60).time();
        $user->commission_rate = config('insurance.default_merchant_percentage');

        if($user->save())
        {
            $request['status'] = true;
            $request['message'] = 'Account was successfully created. An email has been sent to you. PLease check it to continue';
            $request["user"] = $user;
        }
        else
        {
            $request['status'] = false;
            $request['message'] = 'User was not created. An error occured';
        }

        return $request;

    }

    function activateCustomer($code)
    {
        $customer =  User::where('confirmation_token', $code)->first();
        if($customer)
        {
            if($customer->status > 1)
            {
                return -1;
            }
            $customer->status = 2;
            $customer->updated_at = date('Y-m-d H:i:s',time());
            if($customer->save())
            {
                $this->user = $customer;
                /*$setting = Setting::where("status",1)->first();
                if($setting)
                {
                    if($setting->give_bonus == 1)
                    {
                        $ca = CustomerAccount::where('CustomerId',$customer->CustomerId)->first();
                        $ca->BonusBalance = $setting->bonus;
                        $this->cus = $customer;
                        $this->ca = $ca;
                        $ca->save();
                    }

                }*/
                return 0;
            }
            return -2;

        }
        return -3;
    }

    public function addUserAdmin()
    {
        $result = array();
        $fname = \Input::get('first_name');
        $last_name = \Input::get('last_name');
        $middle_name = \Input::get('middle_name');
        $company_id = \Input::get('company_id');
        $mobile_no = \Input::get('mobile_no');
        $role_id = \Input::get('role_id');
        $email = \Input::get('email');
        $password = \Input::get('password');

        $user = new User();
        $user->first_name = $fname;
        $user->last_name = $last_name;
        $user->middle_name = $middle_name;
        $user->company_id = $company_id;
        $user->role_id = $role_id;
        $user->status = 2;
        $user->mobile_number = $mobile_no;
        $user->email = $email;
        $user->password = \Hash::make($password);
        $user->created_at = date('Y-m-d H:i:s',time());
        $user->updated_at = date('Y-m-d H:i:s',time());
        $user->confirmation_token = str_random(60).time();

        if($user->save())
        {
            $request['status'] = true;
            $request['message'] = 'User was successfully created.';
        }
        else
        {
            $request['status'] = false;
            $request['message'] = 'User was not created. An error occured';
        }

        return $request;


    }

    function updatePassword()
    {
        $result = array();
        $oldPassword = \Input::get('old_password');
        $newPassword = \Input::get('new_password');

        $users = \Auth::user();
        $user = User::where('email',$users->email)->first();
        if(!$user || !\Hash::check($oldPassword, $user->password))

        {
            $request['status'] = false;
            $request['message'] = 'wrong old password. Please check and try again';
            $request['url'] = null;
            return $request;
        }

        $user->password= \Hash::make($newPassword);

        if($user->save())
        {
            $request['status'] = true;
            $request['message'] = 'password successfully saved';
            $request['url'] = null;
            return $request;
        }

        $request['status'] = false;
        $request['message'] = 'password was not saved';
        $request['url'] = null;
        return $request;


    }
    public function loginService()
    {
        $result = array();

        $email = \Input::get('email');
        $password = \Input::get('password');

        if(empty($email))
        {
            $request['status'] = false;
            $request['message'] = 'Please input your email address';
            $request['url'] = null;
            return $request;
        }

        if(empty($password))
        {
            $request['status'] = false;
            $request['message'] = 'Please input your password';
            $request['url'] = null;
            return $request;
        }

        $user = User::where('email',$email)->first();
        if(!$user || !\Hash::check($password, $user->password))

        {
            $request['status'] = false;
            $request['message'] = 'invalid email address or password';
            $request['url'] = null;
            return $request;
        }
        if($user->status == 1)
        {
            $request['status'] = false;
            $request['message'] = 'Sorry! You have not activated your account. Kindly check your email for the activation mail';
            $request['url'] = null;
            return $request;
        }
        if($user->status == 3)
        {
            $request['status'] = false;
            $request['message'] = 'Sorry! Your account has been blocked. Please contact administrator for more information';
            $request['url'] = null;
            return $request;
        }

            \Auth::loginUsingId($user->user_id);
            $request['status'] = true;
            $request['message'] = 'Sorry! You have not activated your account. Kindly check your email for the activation mail';
        $role = $user->role()->first();
            $request['url'] = $role->role_name."/dashboard";
            return $request;




    }
}