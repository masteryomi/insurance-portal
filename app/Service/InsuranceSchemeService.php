<?php


namespace App\Service;
use App\InsuranceScheme;

class InsuranceSchemeService
{

    public function getAllInsuranceSchemes()
    {
        return InsuranceScheme::all();
    }
}