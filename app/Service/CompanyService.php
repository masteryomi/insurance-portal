<?php
/**
 * Created by PhpStorm.
 * User: Abayomi
 * Date: 8/21/2015
 * Time: 5:12 PM
 */

namespace App\Service;
use App\Company;
use App\CompanyType;


class CompanyService
{

    public function getAllCompanyTypes()
    {
        return CompanyType::all();
    }

    public function getAllCompanies()
    {
        return Company::paginate(10);
    }

    public function findCompany($companyId)
    {
        return Company::find($companyId);
    }

    public function addCompany()
    {
        $name = \Input::get('company_name');
        $address = \Input::get('company_address');
        $contact = \Input::get('contact_person');
        $mobile = \Input::get('mobile_no');
        $email = \Input::get('email');
        $logo = \Input::file('logo');
        $company_type = \Input::get('company_type_id');

        //add to the database
        $company = new Company();
        $company->company_type_id = $company_type;
        $company->company_logo = time()."_".$logo->getClientOriginalName();
        $company->status = 1;
        $company->contact_person_email = $email;
        $company->contact_person_mobile = $mobile;
        $company->contact_person = $contact;
        $company->company_name = $name;
        $company->company_address = $address;
        $company->created_at = date('Y-m-d H:i:s',time());
        $company->updated_at = date('Y-m-d H:i:s',time());
        $company->created_by = \Auth::user()->user_id;
        $logo->move( base_path().'/'.config('insurance.company_logo_path'),$company->company_logo);

        $msg = array();
        if($company->save())
        {
            $msg['status'] = true;
            $msg['message'] = 'A company with name <b>'.$company->company_name.'</b> has been created successfully';
        }

        else
        {
            $msg['status'] = false;
            $msg['message'] = 'Company could not be created. Please try again';
        }
        return $msg;


    }
}