<?php
/**
 * Created by PhpStorm.
 * User: Abayomi
 * Date: 8/22/2015
 * Time: 11:00 PM
 */

namespace App\Service;
    use App\InsuranceScheme;
    use App\InsuranceSchemeType;
    use App\Offering;

    class ProductService
{
    public function getAllPolicies()
    {
        return InsuranceScheme::all();
    }

    public function getInsurancePolicy($id)
    {
        return InsuranceScheme::find($id)->first();
    }
        public function updateOffering()
        {
            $oid =  \Input::get('offering_id');
            //$ipid =  \Input::get('insurance_policy_id');
            $name =  \Input::get('name');
            $description =  \Input::get('description');
            $percentage =  \Input::get('percentage');
            $costpercentage =  \Input::get('cost_percentage');
            $fixedprice =  \Input::get('fixed_price');

            if($fixedprice == 0 && $percentage == 0)
            {
                return "You must set at least fixed price or the percentage";
            }

            if($fixedprice != 0 && $percentage != 0)
            {
                return "You must set ONLY one of either fixed price or  percentage";
            }
            $offering = Offering::where('id',$oid)->first();
            if($offering->name != $name)
            {
                $check = Offering::where('company_id',$offering->company_id)->where('insurance_policy_id',$offering->insurance_policy_id)->where('name',$name)->count();
                if($check > 0)
                {
                    return "A product with $name already exist for the selected insurance policy";
                }

            }


            $offering->name = $name;
            $offering->description= $description;
            $offering->percentage = $percentage;
            $offering->cost_percentage = $costpercentage;
            $offering->fixed_price = $fixedprice;
            $offering->updated_at  = date('Y-m-d H:i:s',time());

            if($offering->save())
            {
                return null;
            }
            return "an error occurued while trying to save product";
        }
    public function addOffering()
    {
        $cid =  \Input::get('company_id');
        $ipid =  \Input::get('insurance_policy_id');
        $name =  \Input::get('name');
        $description =  \Input::get('description');
        $percentage =  \Input::get('percentage');
        $costpercentage =  \Input::get('cost_percentage');
        $fixedprice =  \Input::get('fixed_price');

        if($fixedprice == 0 && $percentage == 0)
        {
            return "You must set at least fixed price or the percentage";
        }

        if($fixedprice != 0 && $percentage != 0)
        {
            return "You must set ONLY one of either fixed price or  percentage";
        }
        $exist = Offering::where('company_id',$cid)->where('insurance_policy_id',$ipid)->where('name',$name)->count();
        if($exist > 0)
        {
            return "A product with $name already exist for the selected insurance policy";
        }

        $offering = new Offering();
        $offering->company_id = $cid;
        $offering->insurance_policy_id = $ipid;
        $offering->name = $name;
        $offering->description= $description;
        $offering->percentage = $percentage;
        $offering->cost_percentage = $costpercentage;
        $offering->fixed_price = $fixedprice;
        $offering->updated_at = $offering->created_at = date('Y-m-d H:i:s',time());

        if($offering->save())
        {
            return null;
        }
        return "an error occurued while trying to save product";
    }

    public function addPolicy()
    {
        $name = \Input::get('scheme_name');
        $description = \Input::get('scheme_description');
        $logo  = \Input::file('logo');


        $is = new InsuranceScheme();
        $is->scheme_name = $name;
        $is->scheme_description = $description;
        $is->created_at = date('Y-m-d H:i:s',time());
        $is->updated_at = date('Y-m-d H:i:s',time());
        $is->created_by = \Auth::user()->user_id;
        $is->is_scheme_created_by_admin = 1;
        $is->status = 1;
        $is->logo = time()."_".$logo->getClientOriginalName();
        $logo->move( base_path().'/'.config('insurance.policy_logo_path'),$is->logo);

        $msg = array();
        if($is->save())
        {
            $msg['status'] = true;
            $msg['message'] = 'A policy '.$is->scheme_name.' has been created successfully';
        }

        else
        {
            $msg['status'] = false;
            $msg['message'] = 'Policy could not be created. Please try again';
        }
        return $msg;


    }

    public function addPolicyPackage()
    {
        $name = \Input::get('scheme_type_name');
        $description = \Input::get('scheme_type_description');
        $id  = \Input::get('scheme_id');


        $is = new InsuranceSchemeType();
        $is->scheme_type_name = $name;
        $is->scheme_type_description = $description;
        $is->created_at = date('Y-m-d H:i:s',time());
        $is->updated_at = date('Y-m-d H:i:s',time());
        $is->created_by = \Auth::user()->user_id;
        $is->is_scheme_type_created_by_admin = 1;
        $is->status = 1;
        $is->insurance_scheme_id = $id;


        $msg = array();
        if($is->save())
        {
            $msg['status'] = true;
            $msg['message'] = 'A policy '.$is->scheme_name.' has been created successfully';
        }

        else
        {
            $msg['status'] = false;
            $msg['message'] = 'Policy could not be created. Please try again';
        }
        return $msg;


    }

}