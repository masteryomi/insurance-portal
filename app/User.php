<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    protected $primaryKey='user_id';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function role()
    {
        return $this->belongsTo('App\Role','role_id','role_id');
    }

    public function user_status()
    {
        return $this->hasOne('App\Status','status_id','status');
    }

    public function created_products()
    {
        return $this->hasMany('App\InsuranceScheme','user_id');
    }
    public function created_packages()
    {
        return $this->hasMany('App\InsuranceSchemeType','user_id');
    }

    public function full_name()
    {
        return $this->first_name ." ".$this->last_name;
    }
}
