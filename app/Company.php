<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table='companies';
    protected $primaryKey='company_id';

    public function company_type()
    {
        return $this->hasOne('App\CompanyType','company_type_id','company_type_id');
    }
    public function policies()
    {
        return $this->hasMany('App\Offering','company_id','company_id');
    }

    public function insurance_schemes()
    {
        return $this->hasMany('App\Offering','company_id','company_id')->get();
    }
}
