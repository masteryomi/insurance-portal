<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offering extends Model
{
    protected $table='offerings';
    protected $primaryKey='id';

    public function policies()
    {
        return $this->hasOne('App\Company','company_id','id');
    }

    public function company()
    {
        return $this->hasOne('App\Company','company_id','company_id');
    }
    public function sold()
    {
        return $this->hasMany('App\UserOffering','offerings_id','id');
    }

    public function insurancepolicy()
    {
        return $this->hasOne('App\InsurancePolicy','id','insurance_policy_id');
    }

    public function features()
    {
        return $this->hasMany('App\OfferingFeature','offerings_id','id');
    }

    public function getSimilarOfferings()
    {
        return \DB::table('offerings')
            ->join('companies', 'offerings.company_id', '=', 'companies.company_id')
            ->where('insurance_policy_id',$this->insurance_policy_id)
            ->where('offerings.id','<>',$this->id);
    }

    public function document_to_upload()
    {
        return \DB::table('document_types')
            //->join('offering_document_types', 'offerings.id', '=', 'offering_document_types.offering_id')
            ->join('offering_document_types','offering_document_types.document_type_id','=','document_types.id')
            ->where('offering_document_types.offering_id','=',$this->id);
    }

    public function has_document_been_uploaded($dtid)
    {

    }



}
