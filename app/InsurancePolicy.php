<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsurancePolicy extends Model
{
    protected $table='insurance_policies';
    protected $primaryKey='id';

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public function offerings()
    {
        return $this->hasMany('App\Offering','insurance_policy_id','id');
    }
}
