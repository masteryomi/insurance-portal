<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferingFeature extends Model
{
    protected $table='offering_features';
    protected $primaryKey='id';

    public function offering()
    {
        return $this->hasOne('App\Offering','offerings_id','id');
    }
}
