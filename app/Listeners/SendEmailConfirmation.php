<?php namespace App\Listeners;

use App\Events\UserAccountCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Contracts\Mail\Mailer;

class SendEmailConfirmation {

    protected $mailer;
	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mail)
	{
		$this->mailer = $mail;
	}


	public function handle(UserAccountCreated $event)
	{
        $this->mailer->send('emails.newaccount',['user'=>$event->user],function($message)  use ($event){
            $message->to($event->user->email)->subject(config('insurance.email_subject'));
            $message->from(config('insurance.email_from'), $event->user->first_name." ".$event->user->last_name);
        });
	}

}
