<?php namespace App\Listeners;

use App\Events\AccountActivatedEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Contracts\Mail\Mailer;

class SendEmailForActivation {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
    protected $mailer;
    public function __construct(Mailer $mail)
    {
        $this->mailer = $mail;
    }

	/**
	 * Handle the event.
	 *
	 * @param  AccountWasActivated  $event
	 * @return void
	 */
	public function handle(AccountActivatedEvent $event)
	{
        $this->mailer->send('emails.activation',['user'=>$event->user],function($message)  use ($event){
            $message->to($event->user->email)->subject(config('insurance.email_subject_activation'));
            $message->from(config('insurance.email_from'), $event->user->first_name." ".$event->user->last_name);
        });
	}

}
