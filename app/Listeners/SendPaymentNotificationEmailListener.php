<?php

namespace App\Listeners;

use App\Events\SendPaymentNotificationEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
class SendPaymentNotificationEmailListener
{
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SendPaymentNotificationEmailEvent  $event
     * @return void
     */
    public function handle(SendPaymentNotificationEmailEvent $event)
    {
        try{
            $this->mailer->send('emails.paymentnotification',['offering'=>$event->userOffering],function($message)  use ($event){
                $message->to($event->userOffering->customer()->first()->email)->subject('Update on Order '.$event->userOffering->reference_no);
                $message->from(config('insurance.email_from'), $event->userOffering->customer()->first()->first_name." ".$event->userOffering->customer()->first()->last_name);
            });
        }catch(\Exception $e)
        {

        }

    }
}
