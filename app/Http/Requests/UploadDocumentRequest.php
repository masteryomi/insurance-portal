<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'doc' => 'required|mimes:jpg,png,jpeg,gif,pdf',

        ];
    }

    public function messages()
    {
        return [
            'logo.mimes'    => 'Please upload a valid file. Acceptable format include jpg,png,gif and pdf',




        ];
    }
}
