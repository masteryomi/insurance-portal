<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'scheme_name' => 'required|unique:insurance_schemes,scheme_name',
            'scheme_description' => 'required',
            'logo' => 'mimes:jpg,png,jpeg,gif',

        ];
    }

    public function messages()
    {
        return [
            'logo.mimes'    => 'Please upload a valid image file',

            'scheme_name.unique' => 'A policy with that name already exist',
            'scheme_description.required' => 'The policy description is required',


        ];
    }
}
