<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddOfferingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company_id' => 'required|exists:companies,company_id|regex:/^[0-9]+$/',
            'insurance_policy_id' => 'required|regex:/^[0-9]+$/|exists:insurance_policies,id',
            'name' => 'required|regex:/^[A-Za-z\\s]+$/',
            'description' => 'required|regex:/^[A-Za-z\\s]+$/',


        ];
    }

    public function messages()
    {
        return [
            'company_id.required'    => 'company is required',

            'scheme_name.unique' => 'A policy with that name already exist',
            'scheme_description.required' => 'The policy description is required',


        ];
    }
}
