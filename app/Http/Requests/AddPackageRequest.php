<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddPackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'scheme_type_name' => 'required|unique:insurance_scheme_types',
            'scheme_type_description' => 'required',
            'scheme_id' =>'required|regex:/^[0-9]+$/'

        ];
    }

    public function messages()
    {
        return [

            'scheme_type_name.unique' => 'A package with that name already exist',
            'scheme_type_description.required' => 'The package description is required',
            'scheme_id.regex' => 'We cannot find the selected policy. please go back and try again'


        ];
    }
}
