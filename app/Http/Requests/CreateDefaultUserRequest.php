<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateDefaultUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_type_id' => 'required|digits_between:1,2|integer',
            'email' => 'required|email',
            'mobile_no' => 'required|regex:/^[0]{1}[7-9]{1}[0-1]{1}[0-9]{8}/',
            'company_name' => 'required|unique:companies,company_name',
            'company_address' => 'required',
            'contact_person' => 'required',
            'logo' => 'mimes:jpg,png,jpeg,gif',

        ];
    }

    public function messages()
    {
        return [
            'logo.mimes'    => 'Please upload a valid image file',
            'contact_person.required' => 'The contact person\'s name if required',
            'company_name.unique' => 'A company with the name already exist!',
            'email.required' => 'The email address of contact person is required',
            'mobile_no.unique' => 'The mobile number of contact person is required',
            'company_address.required' => 'A company address is required!',
            'email.required' => 'An email address is required for contact person',

        ];
    }
}
