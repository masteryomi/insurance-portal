<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'mobile_no' => 'required|regex:/^[0]{1}[7-9]{1}[0-1]{1}[0-9]{8}/',
            'first_name' => 'required',
            'last_name' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'email.required'    => 'Please upload a valid image file',
            'contact_person.required' => 'The contact person\'s name if required',
            'company_name.unique' => 'A company with the name already exist!',
            'email.required' => 'Your email address is required',

            'first_name.required' => 'Please enter your first name',
            'last_name.required' => 'Please enter your last name',
            'email.unique' => 'A customer with the email already exist.',
            'password.required' => 'Please input your password',
            'password.same' => 'your password does not match. Please check and try again',

        ];
    }
}
