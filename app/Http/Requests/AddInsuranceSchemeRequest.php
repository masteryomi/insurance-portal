<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 11/10/2015
 * Time: 20:25
 */

namespace App\Http\Requests;
use App\Http\Requests\Request;

class AddInsuranceSchemeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'features' => 'required',
            'insurance_scheme_id' => 'required|regex:/^[0-9]+$/',
            'scheme_type_description' => 'required',
            'is_minimum_set' =>'required',
            'scheme_name' =>'required',
            'company_id' =>'required|exists:companies,company_id|regex:/^[0-9]+$/',
            'logo' => 'mimes:jpg,png,jpeg,gif',
            'rate' => 'max:100|min:1|required',

        ];
    }

    public function messages()
    {
        return [
            'logo.mimes'    => 'Please upload a valid image file',
            'scheme_type_name.unique' => 'A package with that name already exist',
            'scheme_type_description.required' => 'The package description is required',
            'scheme_id.regex' => 'We cannot find the selected policy. please go back and try again'


        ];
    }
}