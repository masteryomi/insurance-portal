<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'old_password' => 'required|min:6',
            'new_password' => 'required|same:new_password2|min:6',


        ];
    }

    public function messages()
    {
        return [
            'new_password.min' => 'Password must be minimum of 6 digits',
            'old_password.required' => 'Please input your old password',
            'new_password.required' => 'Please input your new password',
            'new_password.same' => 'your password does not match. Please check and try again',

        ];
    }
}
