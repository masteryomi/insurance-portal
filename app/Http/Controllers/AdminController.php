<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function dashboard()
    {
        compact("user",\Auth::user());
        return view('admin.dashboard')->with('user',\Auth::user());
    }

    public function sign_out()
    {
        \Auth::logout();
        return redirect('guest/login')->with('success', 'You have successfully logged out');
    }

}
