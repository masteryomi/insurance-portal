<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserOffering;
use App\Payment;
use App\Category;
use App\Company;
use App\PaymentDocument;
class UserPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function detail($id)
    {
        $totalPayment = Payment::where('user_id',\Auth::user()->user_id)
            ->where('payment_id',$id)->first();

        if($totalPayment == null)
        {
            return redirect()->back()->with('error','Payment information was not foiund');
        }

        return view('user.payment.details')->with('user',\Auth::user())->with('payment',$totalPayment);
    }
    public function index()
    {
        $totalPayment = Payment::where('user_id',\Auth::user()->user_id)->paginate(10);
        return view('user.payment.index')->with('payment',$totalPayment);
    }

    public function upload_document(Requests\UploadDocumentRequest $request)
    {
        $pid = \Input::get('payment_id');
        $docid = \Input::get('doc_id');
        $logo  = \Input::file('doc');

        //check if this docu has been uploaded before
        $exist = PaymentDocument::where('payment_id',$pid)->where('document_type_id',$docid)->count();
        if($exist == 0)
        {
            //upload document

            $pd = new PaymentDocument();
            $pd->payment_id = $pid;
            $pd->document_type_id = $docid;
            $pd->document_name = time()."_".$logo->getClientOriginalName();
            $pd->document_path =  base_path().'/'.config('insurance.document_path');
            $pd->date_uploaded = date('Y-m-d H:i:s',time());

            if($pd->save())
            {
                $logo->move( base_path().'/'.config('insurance.document_path'),$pd->document_name);
                return redirect()->back()->with('success','File upload was successful');
            }
            return redirect()->back()->with('error','Error occured. Please try again');

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
