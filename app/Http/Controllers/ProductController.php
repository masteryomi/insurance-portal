<?php

namespace App\Http\Controllers;

use App\Offering;
use App\Service\ProductService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\InsurancePolicy;
use App\Company;
use App\OfferingFeature;

class ProductController extends Controller
{
    protected $productService;
    public function __construct(ProductService $service)
    {
        $this->middleware('auth');
        $this->productService = $service;
    }

    public function manage_product()
    {
        return view('admin.products.all')->with('policy',$this->productService->getAllPolicies())->with('user',\Auth::user());
    }
    public function add_products(Requests\AddProductRequest $request)
    {
        $msg = $this->productService->addPolicy();
        if($msg['status'])
        {
            return back()->with('success',$msg['message']);
        }
        else
        {
            return back()->with('error',$msg['message'])->withInput();
        }
    }
    public function add_new_package(Requests\AddPackageRequest $request)
    {
        $msg = $this->productService->addPolicyPackage();
        if($msg['status'])
        {
            return back()->with('success',$msg['message']);
        }
        else
        {
            return back()->with('error',$msg['message'])->withInput();
        }
    }

    public function add_product()
    {
        return view('admin.products.add')->with('user',\Auth::user());
    }
    public function add_package($id)
    {
        return view('admin.products.add_package')->with('user',\Auth::user())
            ->with('policy',$this->productService->getInsurancePolicy($id));
    }

    public function view_packages($id)
    {
        return view('admin.products.packages')->with('user',\Auth::user())->with('policy',$this->productService->getInsurancePolicy($id));
    }

    public function add_offering($id)
    {

        return view('admin.products.addoffering')->with('companies',Company::all())
            ->with('ip',InsurancePolicy::all())->with('company',Company::where('company_id',$id)->first())->with('user',\Auth::user());
    }
    public function edit_offering($id)
    {

        return view('admin.products.editoffering')->with('offering',Offering::where('id',$id)->first())
          ->with('user',\Auth::user());
    }
    public function update_offering()
    {
        $msg = $this->productService->updateOffering();
        if($msg == null)
        {
            return back()->with('success','Product saved successfully.');
        }
        else
        {
            return back()->with('error',$msg)->withInput();
        }
    }

    public function save_offering(Requests\AddOfferingRequest $request)
    {
        $msg = $this->productService->addOffering();
        if($msg == null)
        {
            return back()->with('success','Product saved successfully. Do not forget to add features for the new product');
        }
        else
        {
            return back()->with('error',$msg)->withInput();
        }
    }

    public function view_features($id)
    {
        return view('admin.products.viewfeatures')->with('offering',Offering::where('id',$id)->first())
            ->with('user',\Auth::user());
    }

    public function add_features()
    {
        $oid = \Input::get('id');
        $feature = \Input::get('feature');

        $of = new OfferingFeature();
        $of->offerings_id = $oid;
        $of->description = $feature;
        $of->created_at = $of->updated_at =  date('Y-m-d H:i:s',time());

        if($of->save())
        {
            return back()->with('success','Feature added successfully')->with('id',$oid);
        }
        else
        {
            return back()->with('error','A fatal error occured while creating the feature')->withInput();
        }

    }


}
