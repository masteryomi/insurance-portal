<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service\UserService;

class UserProfileController extends Controller
{
    protected $userService;
    public function __construct(UserService $userservice)
    {
        $this->middleware('auth');
        $this->middleware('role');
        $this->userService = $userservice;
    }

    function profile()
    {
        $user = \Auth::user();
      return view('user.profile')->with('user',$user);
    }

    function update_profile(Requests\ProfileUpdateRequest $request)
    {
        $resp = $this->userService->updateUser();
        if($resp["status"]== true)
        {

            return redirect('user/profile')->with('success','Your profile update was successful');
        }
        else
        {
            return redirect()->back()->with('error',$resp['message'])->withInput();
        }
    }

    function update_password(Requests\UpdatePasswordRequest $request)
    {
        $resp = $this->userService->updatePassword();
        if($resp["status"]== true)
        {

            return redirect('user/profile')->with('success','Your password was successfully changed');
        }
        else
        {
            return redirect()->back()->with('error',$resp['message'])->withInput();
        }
    }
}
