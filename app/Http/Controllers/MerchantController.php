<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service\RoleService;
use App\Service\CompanyService;
use App\Service\UserService;
use App\Offering;
use App\UserOffering;
use App\Payment;
use App\Category;
use App\Company;

class MerchantController extends Controller
{
    protected $roleService;
    protected $companyService;
    protected $userService;

    public function __construct(RoleService $roleService,CompanyService $cservice,UserService $userService)
    {
        //$this->middleware('auth');
        $this->roleService = $roleService;
        $this->companyService = $cservice;
        $this->userService = $userService;
    }
    public function dashboard()
    {
        $totalPayment = Payment::where('user_id',\Auth::user()->user_id)->take(5)->get();
        $product = UserOffering::where("user_id",\Auth::user()->user_id)->orderBy('date_requested', 'desc')->take(5)->get();
        $uo = UserOffering::where('user_id',\Auth::user()->user_id)->where('response_code','!=','00')->count();
        return view('merchant.dashboard')->with('user',\Auth::user())->with('payment',$totalPayment)->with('product',$product);
    }

    public function sign_out()
    {
        \Auth::logout();
        return redirect('guest/')->with('success', 'You have successfully logged out');
    }
}
