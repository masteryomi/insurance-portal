<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service\RoleService;
use App\Service\CompanyService;
use App\Service\UserService;
use App\Offering;
use App\UserOffering;
use App\Payment;
use App\Category;
use App\Company;

class UserController extends Controller
{
    protected $roleService;
    protected $companyService;
    protected $userService;

    public function __construct(RoleService $roleService,CompanyService $cservice,UserService $userService)
    {
        //$this->middleware('auth');
        $this->roleService = $roleService;
        $this->companyService = $cservice;
        $this->userService = $userService;
    }
    public function index()
    {
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        return view('user.product.index')->with('category',$category)->with('company',$company);;
    }

    public function add_user()
    {
        $role = $this->roleService->GetAllRoles();
        $company = $this->companyService->getAllCompanies();

        return view('admin.user.create')->with('role',$role)->with('user',\Auth::user())->with('company',$company);
    }

    public function sign_out()
    {
        \Auth::logout();
        return redirect('guest/')->with('success', 'You have successfully logged out');
    }

    public function all()
    {
        return view('admin.user.all')->with('user',\Auth::user())->with('users',$this->userService->all());
    }

    public function dashboard()
    {
        $totalCommission = Payment::where('user_id',\Auth::user()->user_id)->sum('commission');
        $totalPaidCommission = Payment::where('user_id',\Auth::user()->user_id)->whereNotNull('date_commission_paid')->sum('commission');
        $totalPayment = Payment::where('user_id',\Auth::user()->user_id)->take(5)->get();
        $product = UserOffering::where("user_id",\Auth::user()->user_id)->orderBy('date_requested', 'desc')->take(5)->get();
        $uo = UserOffering::where('user_id',\Auth::user()->user_id)->where('response_code','!=','00')->count();
        return view('user.dashboard')->with('user',\Auth::user())->with('payment',$totalPayment)
            ->with('product',$product)->with('commission',$totalCommission)->with('paid',$totalPaidCommission);
    }

}
