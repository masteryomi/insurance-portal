<?php

namespace App\Http\Controllers;

use App\Company;
use App\Events\AccountActivatedEvent;
use Illuminate\Http\Request;

use Event;
use App\Events;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service\UserService;
use App\Category;
use App\Service\RoleService;
use App\Service\CompanyService;
use App\Http\Requests\NewUserRequest;
use App\Events\UserAccountCreated;
class GuestController extends Controller
{
    protected $userService;
    protected $roleService;
    protected $companyService;


    public function __construct(RoleService $roleService,CompanyService $cservice,UserService $userService)
    {

        $this->roleService = $roleService;
        $this->companyService = $cservice;
        $this->userService = $userService;
    }


    public function login()
    {
        return view('guest.login');
    }
    public function index()
    {
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        return view('guest.index')->with('category',$category)->with('company',$company);;
    }

    public function contactus()
    {
        return view('guest.contact');
    }

    public function faq()
    {
        return view('guest.faq');
    }
    public function findagent()
    {
        return view('guest.agent');
    }
    public function aboutus()
    {
        return view('guest.aboutus');
    }

    public function user_login()
    {
        $msg = $this->userService->loginService();
        if(!$msg['status'])
        {
            return back()->with('error',$msg['message'])->withInput();
        }
        return redirect()->intended($msg['url']);
    }

    public function register()
    {

      return view('guest.register');
    }

    public function register_merchant()
    {

        return view('guest.registermerchant');
    }

    public function register_account(NewUserRequest $request)
    {
        $resp = $this->userService->addUser();
        if($resp["status"]== true)
        {
            Event::fire(new UserAccountCreated($resp["user"]));
            return redirect('success')->with('message','<b>Congratulations! </b>
                    You have successfully created your account. You have been sent an email. Kindly check it and follow the instructions.<br>
                We hope to see you soon!');
        }
        else
        {
            return redirect()->back()->with('error','Account could not be created. Please try again later')->withInput();
        }


    }

    public function register_account_merchant(NewUserRequest $request)
    {
        $resp = $this->userService->addUserMerchant();
        if($resp["status"]== true)
        {
            Event::fire(new UserAccountCreated($resp["user"]));
            return redirect('success')->with('message','<b>Congratulations! </b>
                    You have successfully created your account. You have been sent an email. Kindly check it and follow the instructions.<br>
                We hope to see you soon!');
        }
        else
        {
            return redirect()->back()->with('error','Account could not be created. Please try again later')->withInput();
        }


    }

    function activate_account($code)
    {
        $answer = $this->userService->activateCustomer($code);
        if($answer == 0)
        {
            Event::fire(new AccountActivatedEvent( $this->userService->user));
            return redirect('success')->with('message','<b>Congratulations! </b>
                    Your account was successfully verified. Please login to start using the service');
        }
        else if($answer == -1)
        {
            return redirect('failed')->with('message','<b>An error occurred! </b>
                    Your account was previously activated. Please login');
        }
        else if($answer == -2)
        {
            return redirect('failed')->with('message','<b>An error occurred! </b>
                    Your account could not be activated. Please try again');
        }
        else{
            return redirect('failed')->with('message','<b>An error occurred! </b>.
                    Your account could not be found. Please try again or cantact administrator for assistance');
        }

    }
    function success()
    {
        if(\Session::get('message'))
        {
            return view('guest.success')->with('message',\Session::get('message'));
        }
        return view('errors.503');
    }
    function failed()
    {
        if(\Session::get('message'))
        {
            return view('guest.failed')->with('message',\Session::get('message'));
        }
        return view('errors.503');
    }



}
