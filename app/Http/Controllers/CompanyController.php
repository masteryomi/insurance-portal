<?php

namespace App\Http\Controllers;
use App\Service\CompanyService;
use App\Service\InsuranceSchemeService;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class CompanyController extends Controller
{
    protected $companyService;
    protected $insuranceScheme;
    public function __construct(CompanyService $service,InsuranceSchemeService $insuranceScheme)
    {
        $this->companyService = $service;
        $this->insuranceScheme = $insuranceScheme;
    }



    public function add_new_company(CompanyRequest $request)
    {
        $msg = $this->companyService->addCompany();
        if($msg['status'])
        {
            return back()->with('success','You have successfully added a new company');
        }
       else
       {
           return back()->with('error',$msg['message'])->withInput();
       }
    }
    public function add_company()
{
    return view('admin.company.add')->with('ct',$this->companyService->getAllCompanyTypes())
        ->with('user',\Auth::user());
}

    function manage_company()
    {
        return view('admin.company.manage')->with('c',$this->companyService->getAllCompanies())
            ->with('user',\Auth::user());
    }

    function manage_company_products($companyId)
    {
        $company = $this->companyService->findCompany($companyId);
        if($company == null)
        {
            return back()->with('error','Sorry!  Company could not be found');
        }
        if($company->company_type_id != config('insurance.insurance_company'))
        {
            return back()->with('error','Sorry! '.$company->company_name.' was not configured as an insurance company ');
        }
        return view('admin.company.products.products')->with('c',$company)
            ->with('user',\Auth::user())->with('cid',$companyId);
    }

    public function add_company_product($companyId)
    {
        $company = $this->companyService->findCompany($companyId);
        if($company == null)
        {
            return back()->with('error','Sorry! Company could not be found');
        }
        $is = $this->insuranceScheme->getAllInsuranceSchemes();
        return view('admin.company.products.add')->with('c',$company)
            ->with('user',\Auth::user())->with('is',$is);
    }




}
