<?php

namespace App\Http\Controllers;

use App\Events\SendPaymentNotificationEmailEvent;
use App\UserOffering;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Company;
use App\Offering;
use App\Events;
use Event;
use App\Payment;
class UserProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');

    }

    public function buy($id)
    {
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        $product = Offering::where("id",$id)->first();

        return view('user.buy')->with('category',$category)->with('product',$product)->with('company',$company);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function completed()
    {
        if(session()->has('of'))
        {
           // dd(json_encode(session()->get('of')));
            $id = session()->get('of')->offerings_id;
            $product = Offering::where("id",$id)->first();
            return view('user.completed')->with('product',$product);
        }
        else{

            return view('user.completed');
        }

    }
    public function history_detail($id)
    {

        $product = UserOffering::where("user_id",\Auth::user()->user_id)->where('id', $id)->first();
        return view('user.historydetail')->with('product',$product);

    }

    public function history()
    {


            $product = UserOffering::where("user_id",\Auth::user()->user_id)->orderBy('date_requested', 'desc')->paginate(10);
            return view('user.history')->with('product',$product);


    }
    public function complete()
    {
        $transactionId = \Input::get('gtpay_tranx_id');
        $statusCode = \Input::get('gtpay_tranx_status_code');
        $currency = \Input::get('gtpay_tranx_curr');
        $statusMessage = \Input::get('gtpay_tranx_status_msg');
        $amount = \Input::get('gtpay_tranx_amt')/100;
        $customerEmail = \Input::get('gtpay_cust_id');
        $echoData = \Input::get('gtpay_echo_data');
        $gatewayName = \Input::get('gtpay_gway_name');

        if($statusCode == '00')
        {
            $hash = strtoupper(hash('sha512',config('insurance.merchant_id').$transactionId.config('insurance.gtpay_hashkey')));
            $url = config('insurance.gtpay_requery_url'). "mertid=".config('insurance.merchant_id').
                "&amount=" . $amount . "&tranxid=".$transactionId . "&hash=".$hash;

            $handle = fopen($url,'r');
            $answer = fgets($handle,1024);
            $response = json_decode($answer);
            //dd($response);
            if($response->ResponseCode == '00')
            {
                //then the transaction was successful
                $userOffering = UserOffering::where('reference_no',$transactionId)->where('user_id',\Auth::user()->user_id)->first();
                if(!$userOffering)
                {
                    return redirect()->intended('user/completed')->with('response',$response->ResponseDescription)->with('error','Transaction was not found');
                }
                //update the transaction
                $userOffering->approved_amount= $response->Amount/100  ;
                $userOffering->response_code = $statusCode;
                $userOffering->response_description= $statusMessage;
                $userOffering->response_from_switch =  date('Y-m-d H:i:s',time());
                $userOffering->save();

                //also save it to payment table as new payment

                $payment = new Payment();
                $payment->user_id = \Auth::user()->user_id;
                $payment->created_at = $payment->updated_at =date('Y-m-d H:i:s',time());
                $payment->status = 1;
                $payment->user_offering_id = $userOffering->id;
                $payment->offerings_id = $userOffering->offerings_id;
                $payment->sum_insured = $userOffering->sum_insured;
                $payment->total_amount = $userOffering->total_amount;
                $payment->transaction_charge = 0;
                $payment->approved_amount = $response->Amount/100;
                $payment->switch_status = $response->ResponseCode;
                $payment->switch_reference = $response->MerchantReference;
                $payment->switch_description = $response->ResponseDescription;
                $payment->switch_return_time = date('Y-m-d H:i:s',time());

                //calculate commission
                $rate = \Auth::user()->commission_rate;
                if($rate > 0)
                {
                    $off = Offering::where('id', $userOffering->offerings_id)->first();
                    $payment->commission = ($payment->approved_amount * $rate)/$off->percentage;

                }
                $payment->save();


                    //fire email event to customer
                Event::fire(new SendPaymentNotificationEmailEvent($userOffering));

                //redirect user to a page to show transaction was successful
                return redirect()->intended('user/completed')->with('response',$response->ResponseDescription)->with('success','Transaction was completed successfully')->with('of',$userOffering);

            }

        }
        $userOffering = UserOffering::where('reference_no',$transactionId)->where('user_id',\Auth::user()->user_id)->first();
        if(!$userOffering)
        {
           // dd(json_encode(\Input::all()));
            return redirect()->intended('user/completed')->with('response',$statusMessage)->with('error','Transaction was not found')->with('of',$userOffering);
        }
        //update the transaction
        $userOffering->approved_amount= $amount;
        $userOffering->response_code = $statusCode;
        $userOffering->response_description= $statusMessage;
        $userOffering->response_from_switch =  date('Y-m-d H:i:s',time());
        $userOffering->save();
        //fire email event to customer
        Event::fire(new SendPaymentNotificationEmailEvent( $userOffering));
        return redirect('user/completed')->with('response',$statusMessage)->with('error','Transaction was not completed')->with('of',$userOffering);



    }

    public function forwardfinal(Request $request)
    {
        //submit
        $submit = \Input::get('submit');
        if($submit == 'cancel')
        {
            //delete and redirect back to dashboard
            return redirect('user/dashboard');
        }
        else{
            //else take customer to place to pay with card
            //save this payment into the database
            $uo = new UserOffering();
            $uo->created_at = $uo->date_requested = date('Y-m-d H:i:s',time());
            $uo->updated_at = date('Y-m-d H:i:s',time());
            $uo->user_id = \Auth::user()->user_id;
            $uo->offerings_id = $request->session()->get('oid');
            $uo->rebate = 0;
            $uo->status_id = 1;
            $uo->sum_insured = $request->session()->get('sum');
            $uo->total_amount = $request->session()->get('totalamount');

            $uo->reference_no =  $request->session()->get('reference_no');
            $offering = Offering::where('id',$uo->offerings_id)->first();
            if($uo->save())
            {
                return view('user.pay')->with('product',$offering)->with('hash',$request->session()->get('hash'));
            }
            else
            {
                return redirect()->back()->with('error','Sorry, your request could not be saved. Please try again later');
            }

        }
    }

    private function quickRandom($length = 16)
    {
        $pool = '0123456789';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
    public function forward(Request $request)
    {
        $id = \Input::get("oid");


        if(!$id)
        {
            return redirect()->back()->with('error','No policy was selected. please try again');
        }


        //
        $offering = Offering::where('id',$id)->first();
        if(!$offering)
        {
            return redirect()->back()->with('error','The selected policy does not exist any more. Please check and try again');
        }
        $amount = 0;

        if($offering->fixed_price == 0)
        {
            $sum = \Input::get("suminsured");

            if(!is_numeric($sum))
            {
                return redirect()->back()->with('error','Invalid sum insured. Please input a valiid amount');
            }
            if($sum < config('insurance.minimum'))
            {
                return redirect()->back()->with('error','Invalid sum insured. It MUST be greater than or equal to '.config('insurance.minimum'));
            }

            $amount =  round(($sum * ($offering->percentage * 0.01)),2);
        }
        else
        {
            $amount = $offering->fixed_price;
            $sum = $offering->fixed_price;
        }

        $request->session()->put('oid',$id);
        $request->session()->put('sum',$sum);
        $request->session()->put('totalamount',$amount);
        $reference_no = 'E'.$this->quickRandom(19);
        $request->session()->put('reference_no',$reference_no);

        $concatData = config('insurance.merchant_id').''.
            $reference_no.''.($amount * 100).''.config('insurance.currency').''.
            \Auth::user()->user_id.''.config('insurance.return_url').''.config('insurance.gtpay_hashkey');

       //dd($concatData);
        $hash = strtoupper(hash('sha512',$concatData));
        //dd($concatData,$hash);
        $request->session()->put('hash',$hash);
        return view('user.forward')->with('product',$offering)->with('sum',$sum)->with('total',$amount)->with('hash',$hash);

    }

    private function sendHttpRequest($host, $path, $query, $port=80){
        header("POST $path HTTP/1.1\r\n" );
        header("Host: $host\r\n" );
        header("Content-type: application/x-www-form-urlencoded\r\n" );
        header("Content-length: " . strlen($query) . "\r\n" );
        header("Connection: close\r\n\r\n" );
        header($query);
    }


}
