<?php

namespace App\Http\Controllers;

use App\InsurancePolicy;
use App\Offering;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Company;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        //
    }

    public function show_category($id)
    {
        //get all the products under this category
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        $category1 = Category::where('id',$id)->first();
        return view('guest.products.category_products')->with('category',$category)->with('category1',$category1)->with('company',$company);
    }

    public function show_insurer($id)
    {
        //get all the products under this category
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        $company1 = Company::where("company_type_id",2)->where('company_id',$id)->first();
        return view('guest.insurer.products')->with('category',$category)->with('company1',$company1)->with('company',$company);
    }

    public function product($id)
    {
        //get all the products under this category
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        $product = Offering::where("id",$id)->first();
        return view('guest.products.prduct')->with('category',$category)->with('product',$product)->with('company',$company);
    }

    public function products($id)
    {
        //get all the products under this category
        $category = Category::all();
        $company = Company::where("company_type_id",2)->get();
        $product = InsurancePolicy::where("id",$id)->first();
        return view('guest.products.product')->with('category',$category)->with('product',$product)->with('company',$company);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
