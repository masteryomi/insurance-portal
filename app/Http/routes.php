<?php

Route::get('/','GuestController@index');
Route::get('/about-us','GuestController@aboutus');
Route::get('/faq','GuestController@faq');
Route::get('/contact-us','GuestController@contactus');
Route::get('/find-an-agent','GuestController@findagent');



Route::get('guest/','GuestController@index');
Route::get('guest/login','GuestController@login');
Route:post('guest/auth-login','GuestController@user_login');
Route::get('guest/sign-up','GuestController@register');
Route::get('guest/sign-up-merchant','GuestController@register_merchant');
Route::post('guest/create-account','GuestController@register_account');
Route::post('guest/create-account-merchant','GuestController@register_account_merchant');
Route::get('guest/reset-password','GuestController@reset_pass');
Route::get('guest/category/{id}','CategoryController@show_category')->where('id', '[0-9]+');
Route::get('guest/insurer/{id}','CategoryController@show_insurer')->where('id', '[0-9]+');
Route::get('guest/product/{id}','CategoryController@products')->where('id', '[0-9]+');
Route::get('guest/products/{id}','CategoryController@product')->where('id', '[0-9]+');
Route::get('guest/active/{code}', [ 'as' => 'users.activate_account', 'uses' => 'GuestController@activate_account' ]);
Route::get('/success', 'GuestController@success');
Route::get('/failed', 'GuestController@failed');

Route::get('guest/password/email', 'Auth\PasswordController@getEmail');
Route::post('guest/password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('guest/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('guest/password/reset', 'Auth\PasswordController@postReset');



Route::group(['middleware' => ['auth','role']], function () {

    //merchant
    Route::get('merchant/dashboard','MerchantController@dashboard');
    Route::get('merchant/signout','MerchantController@sign_out');

    //user
    Route::get('user/','UserController@index');
    Route::get('user/product/buy/{id}','UserProductController@buy')->where('id', '[0-9]+');
    Route::get('user/dashboard','UserController@dashboard');
    Route::get('user/signout','UserController@sign_out');
    Route::post('user/product/forward','UserProductController@forward');
    Route::post('user/product/forwardfinal','UserProductController@forwardfinal');
    Route::post('user/product/complete','UserProductController@complete');
    Route::get('user/product/complete','UserProductController@complete');
    Route::get('user/product/complete','UserProductController@complete');
    Route::get('user/product/history','UserProductController@history');
    Route::get('user/profile','UserProfileController@profile');
    Route::post('user/update-profile','UserProfileController@update_profile');
    Route::post('user/update-password','UserProfileController@update_password');
    Route::resource('user/complete','UserProductController@complete');
    Route::get('user/completed','UserProductController@completed');
    Route::get('user/history-detail/{id}','UserProductController@history_detail')->where('id', '[0-9]+');;
    Route::get('user/payments/details/{id}','UserPaymentController@detail')->where('id', '[0-9]+');;
    Route::post('user/payments/upload-document','UserPaymentController@upload_document');
    Route::get('user/payments','UserPaymentController@index');


    //admin routes

    Route::get('admin/dashboard','AdminController@dashboard');
    Route::get('admin/company/manage','CompanyController@manage_company');
    Route::get('admin/products/all','ProductController@manage_product');
    Route::get('admin/products/add','ProductController@add_product');
    Route::post('admin/products/add-product','ProductController@add_products');
    Route::get('admin/company/add','CompanyController@add_company');
    Route::post('admin/company/add-company','CompanyController@add_new_company');
    Route::get('admin/logout','AdminController@sign_out');
    Route::get('admin/product/packages/{id}','ProductController@view_packages')->where('id', '[0-9]+');;
    Route::get('admin/products/package/add/{id}','ProductController@add_package')->where('id', '[0-9]+');
    Route::get('admin/users/create','UserController@add_user');
    Route::get('admin/users/all','UserController@all');
    Route::get('admin/company/product/{id}','CompanyController@manage_company_products')->where('id', '[0-9]+');;
    Route::get('admin/company/product/add/{id}','CompanyController@add_company_product')->where('id', '[0-9]+');
    Route::get('admin/company/product/add-offering/{id}','ProductController@add_offering')->where('id','[0-9]+');
    Route::post('admin/company/product/save-offering','ProductController@save_offering');
    Route::get('admin/company/product/edit-offering/{id}','ProductController@edit_offering')->where('id','[0-9]+');
    Route::post('admin/company/product/update-offering','ProductController@update_offering');
    Route::get('admin/offering/view-features/{id}','ProductController@view_features')->where('id','[0-9]+');
    Route::post('admin/offering/add-features/','ProductController@add_features');


    });
