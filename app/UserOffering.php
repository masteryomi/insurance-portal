<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOffering extends Model
{

    public function offering()
    {
        return $this->hasOne('App\Offering','id','offerings_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\User','user_id','user_id');
    }
    public function payments()
    {
        return $this->hasMany('App\Payment','user_offering_id','id');
    }
}
