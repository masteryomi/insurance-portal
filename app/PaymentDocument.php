<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDocument extends Model
{
    protected $table='payment_documents';
    protected $primaryKey='payment_document_id';
    public $timestamps= false;
}
