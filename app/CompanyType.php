<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    protected $table='company_types';
    protected $primaryKey='company_type_id';

    public function companies()
    {
        return $this->hasMany('App\Company','company_type_id');
    }
}
