<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $primaryKey='status_id';

    public function users()
    {
        return $this->hasMany('App\User','status');
    }
}
