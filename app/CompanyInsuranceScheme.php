<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInsuranceScheme extends Model
{
    protected $table='company_insurance_schemes';
    protected $primaryKey='	company_insurance_scheme_id';

    public function companies()
    {
        return $this->hasMany('App\Company','company_id');
    }
}
