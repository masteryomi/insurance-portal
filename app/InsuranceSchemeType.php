<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceSchemeType extends Model
{
    protected $primaryKey='insurance_scheme_type_id';
    protected $table='insurance_scheme_types';

    public function insurance_scheme()
    {
        return $this->hasOne('App\InsuranceScheme','insurance_scheme_id');
    }
    public function createdBy()
    {
        return $this->hasOne('App\User','user_id','created_by');
    }
}
