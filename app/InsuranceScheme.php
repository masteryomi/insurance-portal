<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceScheme extends Model
{
    protected $table='insurance_schemes';
    protected $primaryKey='insurance_scheme_id';

    public function scheme_types()
    {
        return $this->hasMany('App\InsuranceSchemeType','insurance_scheme_id','insurance_scheme_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\User','user_id','created_by');
    }
}
