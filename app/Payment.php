<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table="payments";
    protected $primaryKey = "payment_id";

    public function offering()
    {
        return $this->belongsTo('App\Offering','offerings_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','user_id');
    }

    public function user_offering()
    {
        return $this->belongsTo('App\UserOffering','user_offering_id','id');
    }

    public function document_uploaded()
    {

        return \DB::table('payment_documents')

            ->where('payment_id','=',$this->payment_id);

    }
    public function is_document_uploaded($dtid)
    {

        return \DB::table('payment_documents')

            ->where('payment_id','=',$this->payment_id)->where('document_type_id',$dtid);

    }



}
