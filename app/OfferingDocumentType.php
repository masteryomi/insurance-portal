<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferingDocumentType extends Model
{
    protected $table="offering_document_types";
    protected $primaryKey="offering_document_id";
}
