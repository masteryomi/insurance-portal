@extends('layouts.emails')
@section('content')
<h2 class="title-2">Welcome to {{config('insurance.app_name')}}</h2>

<p>Dear {{$user->first_name}},</p>

<p>Thank you for registering your account. With {{config('insurance.app_name')}}, you can do more with purchasing your insurance policies online.
    You can schedule payments and set reminders.</p>
<p>To be sure that you actually registered for this account, we need you to confirm your registration. Kindly click on the link below :</p>
<p class="callout">
    {{ url('guest/active',  ['code' => $user->confirmation_token])}}

</p>
<p>Please let us know if you need any help</p>

<p>Yours truly</p>
<p>{{config('insurance.app_contact')}}</p>
@stop