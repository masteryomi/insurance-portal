@extends('layouts.emails')
@section('content')
<h2 class="title-2">Thank you for confirming your account with {{config('insurance.app_name')}}</h2>

<p>Dear {{$user->first_name}},</p>

<p>Thank you for confirming your account with {{config('insurance.app_contact')}}, you can do more with purchasing your insurance policies online. You can schedule payments and set reminders.</p>

<p>Please let us know if you need any help</p>

<p>Yours truly</p>
<p>{{config('insurance.app_contact')}}</p>
@stop