@extends('layouts.emails')
@section('content')
    <h2 class="title-2">Sorry that you lost your password!</h2>

    <p>Dear Customer,</p>

    <p>We are sorry that you lost your password. We however can help you reset it but we need to confirm your request</p>
    <p>To be sure that you actually requested for this password reset, kindly click on the link below :</p>
    <p class="callout">
        {{ url('guest/password/reset',  ['token' => $token])}}

    </p>
    <p>Please let us know if you need any help</p>

    <p>Yours truly</p>
    <p>{{config('insurance.app_contact')}}</p>
@stop