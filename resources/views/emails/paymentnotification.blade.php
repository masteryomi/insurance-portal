@extends('layouts.emails')
@section('content')
    <h2 class="title-2">Payment {{$offering->reference_no}} : {{$offering->response_description}}</h2>

    <p>Dear {{$offering->customer()->first()->first_name}},</p>

    <p>We would like to inform you of the status of your transaction details below : </p>

    <p>Please let us know if you need any help</p>

    <p>Yours truly</p>
    <p>{{config('insurance.app_contact')}}</p>
@stop