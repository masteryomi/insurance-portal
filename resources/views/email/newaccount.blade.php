@extends('emails')
@section('content')
    <h2 class="title-2">Welcome to {{config('insurance.app_name')}}</h2>

    <p>Dear {{$user['firstName']}},</p>

    <p>Thank you for registering your account. With 9jarecharge, you can do more with bills payment. You can schedule payments and set reminders.</p>
    <p>To be sure that you actually registered for this account, we need you to confirm your registration. Kindly click on the link below :</p>
    <p class="callout">
        {{ url('users/active',  ['code' => $customer->ConfirmationCode])}}

    </p>
    <p>Please let us know if you need any help</p>

    <p>Yours truly</p>
    <p>Olumide Lawal</p>
@stop