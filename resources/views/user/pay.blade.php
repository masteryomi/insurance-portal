@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">


                        <div class="Ads-Details">
                            <h5 ><strong>Payment Details</strong></h5>
                            <form class="form-horizontal" role="form" method="post" action="{{config("insurance.url")}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <table class="table  table-search" width="100%">
                                    <tr>
                                        <td>
                                            Product Name
                                        </td>
                                        <td>
                                            <b>{{$product->name}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Variance
                                        </td>
                                        <td>
                                            <b>{{$product->insurancepolicy()->first()->name}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Company
                                        </td>
                                        <td>
                                            <b>{{$product->company()->first()->company_name}}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reference Number
                                        </td>
                                        <td>
                                            <b>{{session('reference_no')}}</b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Final Amount
                                        </td>
                                        <td>
                                            @if($product->fixed_price == 0)
                                                <b>&#8358; {{number_format(session('totalamount'),2)}} ( {{number_format($product->percentage,2)}}% of sum insured) </b>
                                            @else
                                                <b>&#8358; {{number_format(session('totalamount'),2)}}</b>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="alert alert-info pgray  alert-lg" role="alert">
                                                <h2 class="no-margin no-padding"><i class="fa fa-info"></i> Important Notice</h2>
                                                <p>Please note that after payment has been confirmed, you will be redirected to a pge to upload the required documents. </p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td colspan="2">
                                            <input type="hidden" name="gtpay_mert_id" value="{{config('insurance.merchant_id')}}" />
                                            <input type="hidden" name="gtpay_tranx_id" value="{{session('reference_no')}}" />
                                            <input type="hidden" name="gtpay_tranx_amt" value="{{session('totalamount') * 100}}" />
                                            <input type="hidden" name="gtpay_tranx_curr" value="{{config('insurance.currency')}}"/>
                                            <input type="hidden" name="gtpay_cust_id" value="{{Auth::user()->user_id}}" />
                                            <input type="hidden" name="gtpay_cust_name" value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}" />
                                            <input type="hidden" name="gtpay_tranx_memo"  value="Payment for {{$product->insurancepolicy()->first()->name}} by {{Auth::user()->email}}" />
                                            <input type="hidden" name="gtpay_echo_data"   value="{{Auth::user()->user_id}}:{{session('totalamount')}}" />
                                            <input type="hidden" name="gtpay_gway_name" value="" />
                                            <input type="hidden" name="gtpay_hash" value="{{session('hash')}}" />
                                            <input type="hidden" name="gtpay_tranx_noti_url" value="{{config('insurance.return_url')}}" />
                                            {{--<input type="hidden" name="gtpay_hash" value="{{session('hash')}}" />--}}
                                            {{--<input type="hidden" name="gtpay_tranx_hash" value="{{session('hash')}}" />--}}
                                            <button class="btn btn-success btn-lg pull-right" name="submit" value="pay"><i class="fa fa-shopping-cart"></i> Process Payment</button>
                                        </td>
                                    </tr>

                                </table>
                            </form>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>

                        <div class="panel sidebar-panel">
                            <div class="panel-heading">Similar Products</div>
                            <div class="panel-content">
                                <div class="panel-body text-left">
                                    <ul class="list-check">
                                        @foreach($product->getSimilarOfferings()->get() as $similar)
                                            <li><a href="{{url("guest/products/")}}/{{$similar->id}}"> {{str_limit($similar->company_name,12)}} (<b>{{str_limit($similar->name,12)}}</b>) - {{$similar->percentage}} %</a></li>
                                        @endforeach


                                    </ul>

                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

@stop