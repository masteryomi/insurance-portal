@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
    <div class="main-container">
    <div class="container">
        <div class="row">
    @include('partials._userlinks')

    <div class="col-sm-9 page-content">

        <div class="inner-box">
            <div class="welcome-msg">
                <h3 class="page-sub-header2 clearfix no-padding">Hello {{$user->first_name}} {{$user->last_name}} </h3>
            </div>
            @include('partials._message')
            <div id="accordion" class="panel-group">
           @if(Auth::user()->commission_rate > 0)
                    <table class="table table-search">
                        <tr>
                            <td>
                                Commission Rate
                            </td>
                            <td>
                                <h3 class="text-success">{{number_format(Auth::user()->commission_rate,2)}} %</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Commission awarded Till Date
                            </td>
                            <td>
                                <h3 class="text-success">&#8358; {{number_format($commission,2)}}</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Commission Paid Till Date
                            </td>
                            <td>
                                <h3 class="text-success">&#8358; {{number_format($paid,2)}}</h3>
                            </td>
                        </tr>
                    </table>
               @endif

            </div>

        </div>

        <div class="inner-box">
            <h3>Recent Insurance Payments</h3>
            <table class="table table-search">
                <tr>
                    <td>Company</td>
                    <td>Product</td>
                    <td>Sum Insured</td>
                    <td>Premium</td>
                    <td>Reference</td>
                    <td> <i class="fa fa-upload" title="Uploaded Documents"></i> </td>
                    <td>Action</td>
                </tr>
                @foreach($payment as $pay)
                    <tr>
                        <td>{{$pay->offering()->first()->company()->first()->company_name}}</td>
                        <td>{{$pay->offering()->first()->name}}</td>
                        <td>&#8358;{{number_format($pay->sum_insured,2)}}</td>
                        <td>&#8358;{{number_format($pay->total_amount,2)}}</td>
                        <td>{{$pay->switch_reference}}</td>
                        <td>{{ $pay->document_uploaded()->count()}}/{{$pay->offering()->first()->document_to_upload()->count()}}</td>
                        <td><p><a href="{{url('user/payments/details')}}/{{$pay->payment_id}}"> <i class="fa fa-search" title="More details"></i></a>

                    </tr>
                    @endforeach
            </table>
        </div>

        <div class="inner-box">
            <h3>Recent Payment</h3>
            <table class="table table-search">
                <thead>
                <tr>
                    <th> Date</th>
                    <th> Sum Insured</th>
                    <th> Product</th>
                    <th> Amount</th>
                    <th>Status</th>
                    <th> Option</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($product as $co)
                    <tr>
                        <th> {{date_format(date_create($co->date_requested),"j-m-Y H:i:s")}}</th>
                        <td>&#8358;{{number_format($co->sum_insured,2)}}</td>
                        <td  class="ads-details-td">{{$co->offering()->first()->name}}-{{$co->offering()->first()->company()->first()->company_name}}
                        </td>
                        <td>&#8358;{{number_format($co->total_amount,2)}}

                        </td>
                        <td >@if($co->response_code == '00')
                                <p class="text-success">Successful</p>
                            @else
                                <p class="text-danger">Failed</p>
                            @endif
                        </td>
                        <td>
                            <div>
                                <p><a class="btn btn-primary btn-xs" href="{{url('user/history-detail')}}/{{$co->id}}"> <i class="fa fa-search" title="More details"></i></a></p>

                            </div>
                        </td>
                    </tr>

                @endforeach


                </tbody>
            </table>
        </div>
    </div>

    </div>

    </div>
    </div>
@stop