@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('partials._userlinks')

                <div class="col-sm-9 page-content">

                    <div class="inner-box">
                        @include('partials._message')
                        <div class="row">
                            <div class="col-md-4 col-sm-4 ">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/index")}}"><i class="fa fa-list ln-shadow"></i> Products <span class="count">{{$category->count()}}</span> </a>
                                        <span data-target=".cat-id-1" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-1">
                                        @foreach ($category as $cat)
                                            <li> <a href="{{url("guest/category/")}}/{{$cat->id}}">{{$cat->name}} ({{$cat->policies()->count()}})</a> </li>

                                        @endforeach
                                    </ul>
                                </div>

                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/1")}}"><i class="icon-ambulance ln-shadow"></i> Life Products<span class="count">&nbsp;</span></a>
                                        <span data-target=".cat-id-3" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-3">
                                        @foreach($category as $cat)
                                            @if($cat->id == 1)
                                                @foreach($cat->policies()->get() as $cats)
                                                    <li> <a href="{{url("guest/product/")}}/{{$cats->id}}">{{$cats->name}} ({{$cats->offerings()->count()}})</a></li>
                                                @endforeach
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/insurer/index")}}"><i class="fa fa-umbrella ln-shadow"></i> Insurers <span class="count">{{$company->count()}}</span></a>
                                        <span data-target=".cat-id-4" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-4">
                                        @foreach($company as $com)
                                            <li> <a href="{{url("guest/insurer/")}}/{{$com->company_id}}">{{$com->company_name}} ({{$com->policies()->count()}})</a> </li>
                                        @endforeach

                                    </ul>
                                </div>


                            </div>
                            <div class="col-md-4 col-sm-4   last-column">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/2")}}"><i class="icon-ambulance ln-shadow"></i> Non-Life Products<span class="count">&nbsp;</span></a>
                                        <span data-target=".cat-id-3" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-3">
                                        @foreach($category as $cat)
                                            @if($cat->id == 2)
                                                @foreach($cat->policies()->get() as $cats)
                                                    <li> <a href="{{url("guest/product/")}}/{{$cats->id}}">{{$cats->name}} ({{$cats->offerings()->count()}})</a></li>
                                                @endforeach
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>

                            </div>
                        </div>


                    </div>

                </div>

            </div>

        </div>
    </div>
@stop