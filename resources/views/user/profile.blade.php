@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('partials._userlinks')

                <div class="col-sm-9 page-content">

                    <div class="inner-box">
                        <div class="welcome-msg">
                            <h3 class="page-sub-header2 clearfix no-padding">Personal Information </h3>
                        </div>
                        <div id="accordion" class="panel-group">
                            @include('partials._message')
                            <form class="form-horizontal" role="form" method="post" action="{{url('user/update-profile')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">First Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="first name" name="first_name" value="{{$user->first_name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Last Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="last name" name="last_name" value="{{$user->last_name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Middle Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="middle name" name="middle_name" value="{{$user->middle_name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mobile Number</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="08099009900" name="mobile_no" value="{{$user->mobile_number}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"> City</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Lagos" name="city" value="{{$user->city}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"> Address</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="address">{{$user->address}}</textarea>
                                    </div>
                                </div>






                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9"> </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">Update</button>
                                    </div>
                                </div>
                            </form>


                        </div>

                    </div>

                    <div class="inner-box">
                        <div class="welcome-msg">
                            <h3 class="page-sub-header2 clearfix no-padding">Change Password </h3>
                        </div>
                        <div id="accordion" class="panel-group">

                            <form class="form-horizontal" role="form" method="post" action="{{url('user/update-password')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Old Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="******" name="old_password" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">New Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="******" name="new_password" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Re-type Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="******" name="new_password2" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9"> </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">Update Password</button>
                                    </div>
                                </div>
                            </form>


                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@stop