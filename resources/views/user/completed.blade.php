@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                @include('partials._userlinks')
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h5 ><strong>Payment Details</strong></h5>
                            @include('partials._message')
                        </div>
                    </div>
                    @if(session()->has('of'))
                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            @if(session()->get('of')->response_code =='00')
                            <h3 class="text-success"><strong>Transaction Successful</strong></h3>
                            @else
                                <h3 class="text-danger"><strong>Transaction Failed</strong></h3>
                                @endif
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Product Name
                                    </td>
                                    <td>
                                        <b>{{$product->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Variance
                                    </td>
                                    <td>
                                        <b>{{$product->insurancepolicy()->first()->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Company
                                    </td>
                                    <td>
                                        <b>{{$product->company()->first()->company_name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Reference Number
                                    </td>
                                    <td>
                                        <b>{{session('reference_no')}}</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Final Amount
                                    </td>
                                    <td>
                                        @if($product->fixed_price == 0)
                                            <b>&#8358; {{number_format(session('totalamount'),2)}} ( {{number_format($product->percentage,2)}}% of sum insured) </b>
                                        @else
                                            <b>&#8358; {{number_format(session('totalamount'),2)}}</b>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status
                                    </td>
                                    <td>
                                        <b>{{session()->get('of')->response_code}} : {{session()->get('of')->response_description}}</b>
                                    </td>
                                </tr>



                            </table>
                        </div>
                    </div>
                    @endif

                </div>



            </div>
        </div>
    </div>

@stop