@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2> {{$product->name}} <small class="label label-default adlistingtype">{{$product->insurancepolicy()->first()->name}}</small> </h2>
                        {{--<span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Today 1:21 pm </span> - <span class="category">{{$product->insurancepolicy()->first()->name}} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> Nigeria </span> </span>--}}


                        <div class="Ads-Details">
                            <h5 class="list-title"><strong>Policy Details</strong></h5>
                            <div class="row">
                                <div class="ads-details-info col-md-8">
                                    <p>{{$product->description}} </p>
                                    <h4>Features</h4>
                                    <ul class="list-circle">
                                        @foreach($product->features()->get() as $feature)
                                            <li>{{$feature->description}}</li>
                                        @endforeach


                                    </ul>

                                </div>
                                <div class="col-md-4">
                                    <aside class="panel panel-body panel-details">
                                        <ul>
                                            <li>
                                                <p class=" no-margin "><strong>Price:</strong>
                                                    @if($product->fixed_price > 0)
                                                        &#8358; {{number_format($product->fixed_price,2)}}
                                                    @else
                                                        {{number_format($product->percentage,2)}}% of sum insured
                                                    @endif
                                                </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Type:</strong> {{$product->insurancepolicy()->first()->name}}</p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Location:</strong> Nigeria </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Brand:</strong> {{$product->company()->first()->company_name}}</p>
                                            </li>
                                        </ul>
                                    </aside>
                                    <div class="ads-action">
                                        <ul class="list-border">
                                            <li><a href="{{url("guest/insurer")}}/{{$product->company()->first()->company_id}}"> <i class=" fa fa-user"></i> More from {{$product->company()->first()->company_name}} </a> </li>
                                            <li><a href="{{url("guest/product")}}/{{$product->insurancepolicy()->first()->id}}"> <i class=" fa fa-heart"></i> More {{$product->insurancepolicy()->first()->name}} products </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="content-footer text-left">
                                <form class="form-horizontal" role="form" method="post" action="{{url('user/product/forward')}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="oid" value="{{ $product->id }}">
                                @if($product->fixed_price == 0)

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Sum Insured</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" placeholder="Sum insured" name="suminsured" value="{{old('suminsured')}}">
                                        </div>
                                    </div>

                                    @endif
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Continue with payment</button>
                                        </div>
                                    </div>

                              </form>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>

                        <div class="panel sidebar-panel">
                            <div class="panel-heading">Similar Products</div>
                            <div class="panel-content">
                                <div class="panel-body text-left">
                                    <ul class="list-check">
                                        @foreach($product->getSimilarOfferings()->get() as $similar)
                                            <li><a href="{{url("guest/products/")}}/{{$similar->id}}"> {{str_limit($similar->company_name,12)}} (<b>{{str_limit($similar->name,12)}}</b>) - {{$similar->percentage}} %</a></li>
                                        @endforeach


                                    </ul>

                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

    @stop