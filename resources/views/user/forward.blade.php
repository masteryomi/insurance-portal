@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">


                        <div class="Ads-Details">
                            <h5 ><strong>Payment Details</strong></h5>
                            <form class="form-horizontal" role="form" method="post" action="{{url('user/product/forwardfinal')}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Product Name
                                    </td>
                                    <td>
                                        <b>{{$product->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Variance
                                    </td>
                                    <td>
                                        <b>{{$product->insurancepolicy()->first()->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Company
                                    </td>
                                    <td>
                                        <b>{{$product->company()->first()->company_name}}</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Sum Insured
                                    </td>
                                    <td>
                                        @if($product->fixed_price == 0)
                                            <b>&#8358; {{number_format(session('sum'),2)}} </b>
                                            @else
                                            <b>&#8358; {{number_format($product->fixed_price,2)}} </b>
                                            @endif

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       Final Amount
                                    </td>
                                    <td>
                                        @if($product->fixed_price == 0)
                                        <b>&#8358; {{number_format(session('totalamount'),2)}} ( {{number_format($product->percentage,2)}}% of sum insured) </b>
                                    @else
                                            <b>&#8358; {{number_format(session('totalamount'),2)}}</b>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="alert alert-info pgray  alert-lg" role="alert">
                                            <h2 class="no-margin no-padding"><i class="fa fa-info"></i> Important Notice</h2>
                                            <p>Please note that after payment has been confirmed, you will be redirected to a page to upload the required documents. </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="2">
                                       <button class="btn btn-danger btn-lg" name="submit" value="cancel"><i class="fa fa-close"></i> Cancel Payment</button>
                                        <button class="btn btn-success btn-lg pull-right" name="submit" value="pay"><i class="fa fa-shopping-cart"></i> Process Payment</button>
                                    </td>
                                </tr>

                            </table>
</form>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>

                        <div class="panel sidebar-panel">
                            <div class="panel-heading">Similar Products</div>
                            <div class="panel-content">
                                <div class="panel-body text-left">
                                    <ul class="list-check">
                                        @foreach($product->getSimilarOfferings()->get() as $similar)
                                            <li><a href="{{url("guest/products/")}}/{{$similar->id}}"> {{str_limit($similar->company_name,12)}} (<b>{{str_limit($similar->name,12)}}</b>) - {{$similar->percentage}} %</a></li>
                                        @endforeach


                                    </ul>

                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

@stop