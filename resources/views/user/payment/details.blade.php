@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                @include('partials._userlinks')
                <div class="col-sm-9 page-content col-thin-right">

                    @if(Auth::user()->commission_rate > 0)
                        <div class="inner inner-box ads-details-wrapper">
                            <div class="Ads-Details">
                                <h3>Commission Information </h3>

                               <table class="table table-search">
                                   <tr>
                                       <td>Commission Rate</td>
                                       <td><b>{{number_format(Auth::user()->commission_rate,2)}}%</b></td>
                                   </tr>
                                   <tr>
                                       <td>
                                           Amount Paid
                                       </td>
                                       <td><b>&#8358; {{number_format($payment->total_amount,2)}}</b></td>
                                   </tr>
                                   <tr>
                                       <td>
                                           Amount Earned
                                       </td>
                                       <td><b>&#8358; {{number_format($payment->commission,2)}}</b></td>
                                   </tr>
                                   <tr>
                                       <td>
                                           Status
                                       </td>
                                       <td>@if($payment->date_commission_paid == null)
                                       <b>Not yet paid</b>
                                       @else
                                       <b>paid on {{date_format(date_create($co->date_commission_paid),"j ls M Y H:i:s")}}</b>
                                       @endif</td>
                                   </tr>
                               </table>
                            </div>
                        </div>
                        @endif
                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                           <h3>Payment Information</h3>
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Payment Reference
                                    </td>
                                    <td>
                                        <b>{{$payment->switch_reference}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Switch Response
                                    </td>
                                    <td>
                                        <b>{{$payment->switch_status}} - {{$payment->switch_Description}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Sum Insured
                                    </td>
                                    <td>
                                        <b>&#8358; {{number_format($payment->sum_insured,2)}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Premium
                                    </td>
                                    <td>
                                        <b>&#8358; {{number_format($payment->total_amount,2)}}</b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3>Product Information</h3>
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Product Name
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       Description
                                    </td>
                                    <td>
                                        {!! wordwrap($payment->offering()->first()->description,30,'<br/>',true) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        % Sum insured
                                    </td>
                                    <td>
                                        <b> {{number_format($payment->offering()->first()->percentage,2)}} %</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Insurance Policy
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->insurancepolicy()->first()->name}}</b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3>Company Information</h3>
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Company Name
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->company()->first()->company_name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address
                                    </td>
                                    <td>
                                        <b>{!! wordwrap($payment->offering()->first()->company()->first()->company_address,30,"<br/>",true) !!}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Person
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->company()->first()->contact_person}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       Contact Mobile
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->company()->first()->contact_person_mobile}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Email
                                    </td>
                                    <td>
                                        <b>{{$payment->offering()->first()->company()->first()->contact_person_email}}</b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3>Document Information</h3>
                            @include('partials._message')
                            <table class="table  table-search" width="100%">
                                @foreach($payment->offering()->first()->document_to_upload()->get() as $doc)
                                <tr>
                                    <td>
                                        {{$doc->name}}
                                    </td>
                                    <td>
                                        {{$doc->description}}
                                    </td>
                                    <td>
                                       @if($payment->is_document_uploaded($doc->id)->count() == 0)
                                            <form class="form-horizontal" role="form" method="post" action="{{url('user/payments/upload-document')}}" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="payment_id" value="{{ $payment->payment_id }}">
                                                <input type="hidden" name="doc_id" value="{{ $doc->id }}">
                                            <input id="input-upload-img1" type="file" class="file" data-preview-file-type="text" name="doc">
                                                <button type="submit" class="btn btn-default btn-sm">Upload</button>
                                                </form>
                                           @else
                                           <b>Uploaded</b>
                                           @endif
                                    </td>
                                </tr>
                                    @endforeach

                            </table>

                        </div>
                    </div>

                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3>Insurance Certificate </h3>

                            @if($payment->status == config('insurance.certificate_status'))
                                <i class="fa fa-2x fa-certificate"></i> Download Certificate
                                @else
                                Certificate is not processed yet
                                @endif
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

@stop