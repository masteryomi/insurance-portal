@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                @include('partials._userlinks')
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3><strong>Insurance Payments</strong></h3>
                            @include('partials._message')
                            <table class="table table-search">
                                <tr>
                                    <td>Company</td>
                                    <td>Product</td>
                                    <td>Sum Insured</td>
                                    <td>Premium</td>
                                    <td>Reference</td>
                                    <td> <i class="fa fa-upload" title="Uploaded Documents"></i> </td>
                                    <td>Action</td>
                                </tr>
                                @foreach($payment as $pay)
                                    <tr>
                                        <td>{{$pay->offering()->first()->company()->first()->company_name}}</td>
                                        <td>{{$pay->offering()->first()->name}}</td>
                                        <td>&#8358;{{number_format($pay->sum_insured,2)}}</td>
                                        <td>&#8358;{{number_format($pay->total_amount,2)}}</td>
                                        <td>{{$pay->switch_reference}}</td>
                                        <td>{{ $pay->document_uploaded()->count()}}/{{$pay->offering()->first()->document_to_upload()->count()}}</td>
                                        <td><p><a href="{{url('user/payments/details')}}/{{$pay->payment_id}}"> <i class="fa fa-search" title="More details"></i></a>

                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="7">
                                        <?php echo $payment->render(); ?>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>


                </div>



            </div>
        </div>
    </div>

@stop