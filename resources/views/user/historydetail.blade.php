@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                @include('partials._userlinks')
                <div class="col-sm-9 page-content col-thin-right">


                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            @if($product->response_code =='00')
                            <h3 class="text-success"><strong>Transaction Successful</strong></h3>
                            @else
                                <h3 class="text-danger"><strong>Transaction Failed</strong></h3>
                                @endif
                            <table class="table  table-search" width="100%">
                                <tr>
                                    <td>
                                        Product Name
                                    </td>
                                    <td>
                                        <b>{{$product->offering()->first()->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Variance
                                    </td>
                                    <td>
                                        <b>{{$product->offering()->first()->insurancepolicy()->first()->name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Company
                                    </td>
                                    <td>
                                        <b>{{$product->offering()->first()->company()->first()->company_name}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Reference Number
                                    </td>
                                    <td>
                                        <b>{{$product->reference_no}}</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Final Amount
                                    </td>
                                    <td>

                                            <b>&#8358; {{number_format($product->total_amount,2)}}</b>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status
                                    </td>
                                    <td>
                                        <b>{{$product->response_code}} : {{$product->response_description}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date Created
                                    </td>
                                    <td>
                                        <b>{{date_format(date_create($product->date_requested),"l jS F Y h:i:s A")}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date Completed
                                    </td>
                                    <td>
                                        <b>{{date_format(date_create($product->updated_at),"l jS F Y h:i:s A")}}</b>
                                    </td>
                                </tr>




                            </table>
                        </div>
                    </div>

                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h3>Actions</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @if($product->response_code != '00')
                                    <a href="{{url('user/product/buy')}}/{{$product->offerings_id}}"><i class="fa fa-refresh"></i> Retry purchase </a>
                                        @else
                                        <a href="{{url('user/payments/details')}}/{{$product->payments()->first()->payment_id}}"><i class="fa fa-search-plus"></i> View complete details </a>
                                    @endif
                                </div>

                            </div>

                            </div>
                        </div>
                </div>



            </div>
        </div>
    </div>

@stop