@extends('layouts.user')
@section('page_title','Welcome')
@section('content')

    <div class="main-container">

        <div class="container">
            <div class="row">
                @include('partials._userlinks')
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <div class="Ads-Details">
                            <h5 ><strong>Payment History</strong></h5>
                            @include('partials._message')
                            <table id="addManageTable" class="table table-search"
                                   data-filter="#filter" data-filter-text-only="true">
                                <thead>
                                <tr>
                                    <th> Date</th>
                                    <th> Reference</th>
                                    <th> Product</th>
                                    <th> Amount</th>
                                    <th>Status</th>
                                    <th> Option</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($product as $co)
                                    <tr>
                                        <th> {{date_format(date_create($co->date_requested),"j/F/Y h:i:s A")}}</th>
                                        <td>{{$co->reference_no}}</td>
                                        <td  class="ads-details-td">{{$co->offering()->first()->name}}-{{$co->offering()->first()->company()->first()->company_name}}
                                        </td>
                                        <td>&#8358;{{number_format($co->total_amount,2)}}

                                        </td>
                                        <td >@if($co->response_code == '00')
                                                <p class="text-success">Successful</p>
                                            @else
                                                 <p class="text-danger">Failed</p>
                                            @endif
                                        </td>
                                        <td>
                                            <div>
                                                <p><a class="btn btn-primary btn-xs" href="{{url('user/history-detail')}}/{{$co->id}}"> <i class="fa fa-search" title="More details"></i></a></p>

                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                <tr>
                                    <td colspan="5">
                                        <?php echo $product->render(); ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>


                </div>



            </div>
        </div>
    </div>

@stop