@extends('layouts.admin')
@section('page_title','Add new Policy')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Edit Offering </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/company/product/update-offering')}}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company</label>
                            <div class="col-sm-9">
                               {{$offering->company()->first()->company_name}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Insurance Policy</label>
                            <div class="col-sm-9">
                                {{$offering->insurancepolicy()->first()->name}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Variant Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="e.g GOLD" name="name" value="{{$offering->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description">{{$offering->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fixed Price</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="0" name="fixed_price" value="{{$offering->fixed_price}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Percentage</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="0" name="percentage" value="{{$offering->percentage}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cost Percentage</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="0" name="cost_percentage" value="{{$offering->cost_percentage}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="hidden" name="offering_id" value="{{$offering->id}}">
                                <button type="submit" class="btn btn-danger">Update Offering</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
@stop