@extends('layouts.admin')
@section('page_title','Add new Policy')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Add Offering </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/company/product/save-offering')}}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company</label>
                            <div class="col-sm-9">
                               <select name="company_id" class="form-control">
                                   @foreach($companies as $co)
                                       @if($co->company_id == old('company_id'))
                                       <option selected="selected" value="{{$co->company_id}}">{{$co->company_name}}</option>
                                       @elseif($co->id ==$company->company_id )
                                           <option selected="selected" value="{{$co->company_id}}">{{$co->company_name}}</option>
                                       @else
                                           <option  value="{{$co->company_id}}">{{$co->company_name}}</option>
                                       @endif
                                   @endforeach
                               </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Insurance Policy</label>
                            <div class="col-sm-9">
                                <select name="insurance_policy_id" class="form-control">
                                    @foreach($ip as $co)
                                        @if($co->id == old('insurance_policy_id'))
                                            <option selected="selected" value="{{$co->id}}">{{$co->name}}</option>
                                        @else
                                            <option  value="{{$co->id}}">{{$co->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Variant Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="e.g GOLD" name="name" value="{{old('name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description">{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fixed Price</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="0" name="fixed_price" value="{{old('fixed_price')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Percentage</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="0" name="percentage" value="{{old('percentage')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cost Percentage</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="0" name="cost_percentage" value="{{old('cost_percentage')}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add Offering</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
@stop