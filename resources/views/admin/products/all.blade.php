@extends('layouts.admin')
@section('page_title','Manage Products')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="fa fa-list "></i> Manage Insurance Policies </h2>
                    <div class="table-responsive">
                        <div class="table-action">
                            <label for="checkAll">
                                <input type="checkbox" onclick="checkAll(this)" id="checkAll">
                                Select: All | <a href="#" class="btn btn-xs btn-danger">Delete <i class="glyphicon glyphicon-remove "></i></a> </label>
                            <div class="table-search pull-right col-xs-7">
                                <div class="form-group">
                                    <label class="col-xs-5 control-label text-right">Search <br>
                                        <a title="clear filter" class="clear-filter" href="#clear">[clear]</a> </label>
                                    <div class="col-xs-7 searchpan">
                                        <input type="text" class="form-control" id="filter">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo"
                               data-filter="#filter" data-filter-text-only="true">
                            <thead>
                            <tr>
                                <th data-type="numeric" data-sort-initial="true"></th>

                                <th> Policy Name</th>
                                <th data-sort-ignore="true">Description</th>
                                <th data-type="numeric"># packages</th>
                                <th data-type="numeric"> Created By</th>
                                <th> Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($policy as $co)
                                <tr>

                                    <td style="width:2%" class="add-img-selector">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="{{$co->insurance_scheme_id}}" name="company_id[]">
                                            </label>
                                        </div>
                                    </td>
                                    <td  class="add-img-td">{{$co->scheme_name}}</td>
                                    <td  class="ads-details-td">{{$co->scheme_description}}</td>
                                    <td  class="ads-details-td">{{$co->scheme_types()->count()}}</td>
                                    <td class="price-td">{{$co->createdBy()->first()->full_name()}}

                                    </td>
                                    <td style="width:10%" class="action-td">
                                        <div>
                                            <p><a class="btn btn-primary btn-xs"> <i class="fa fa-edit"></i> Edit </a></p>

                                            <p><a class="btn btn-primary btn-xs" href="{{url('admin/product/packages')}}/{{$co->insurance_scheme_id}}"> <i class="fa fa-list"></i> Packages </a></p>

                                            <p><a class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete </a></p>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <a class="btn   btn-border btn-post btn-danger" href="{{url('admin/products/add')}}">Add a new policy</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
@stop
@section('script')

    <script type="text/javascript">

    </script>

    <script>
        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for(var i=0; i < chkinput.length; i++) {
                if(chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }

    </script>
@stop