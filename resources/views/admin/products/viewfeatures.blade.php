@extends('layouts.admin')
@section('page_title','Manage Products')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="fa fa-list "></i> Features for {{$offering->company()->first()->company_name}} > {{$offering->name}} </h2>
                    <div class="table-responsive">
                        @if($offering->features()->count() == 0)
                            <div class="alert alert-warning">No feature exist for this insurance policy</div>
                        @else
                        <table id="addManageTable" class="table table-search"
                               data-filter="#filter" data-filter-text-only="true">
                            <thead>
                            <tr>

                                <th> Feature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($offering->features()->get() as $co)
                                <tr>


                                    <td style="width:10%" class="action-td">
                                       {{$co->description}}
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        @endif

                        <h3>Add new Feature</h3>
                            <form class="form-horizontal" role="form" method="post" action="{{url('admin/offering/add-features')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{ $offering->id }}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Feature</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="type explicitly the feature" name="feature" value="{{old('feature')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">Add</button>
                                    </div>
                                </div>
                                </form>

                    </div>
                </div>

            </div>

        </div>

    </div>
@stop
@section('script')

    <script type="text/javascript">

    </script>

    <script>
        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for(var i=0; i < chkinput.length; i++) {
                if(chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }

    </script>
@stop