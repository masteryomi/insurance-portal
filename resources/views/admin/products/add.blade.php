@extends('layouts.admin')
@section('page_title','Add new Policy')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Add new Insurance Policy </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/products/add-product')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Scheme Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Policy Name" name="scheme_name" value="{{old('scheme_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="scheme_description">{{old('scheme_description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Policy Logo</label>
                            <div class="col-sm-9 mb10">
                                <input id="input-upload-img1" type="file" class="file" data-preview-file-type="text" name="logo">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
@stop