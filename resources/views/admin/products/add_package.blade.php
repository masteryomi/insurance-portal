@extends('layouts.admin')
@section('page_title','Add new Policy')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Add Package in {{$policy->scheme_name}} </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/products/package/add-package')}}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Package Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="package name for {{$policy->scheme_name}}" name="scheme_type_name" value="{{old('scheme_type_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Package Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="scheme_type_description">{{old('scheme_type_description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9 mb10">
                                <input  type="hidden"  name="scheme_id" value="{{$policy->insurance_scheme_id}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add Package</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
@stop