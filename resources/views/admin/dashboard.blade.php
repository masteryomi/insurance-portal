@extends('layouts.admin')
@section('page_title','Welcome')
@section('content')
<div class="container">
    <div class="row">
@include('partials._adminlinks')

        <div class="col-sm-9 page-content">
            <div class="inner-box">
                <div class="row">

                    <div class="col-md-12 col-xs-12 col-xxs-12">
                        <div class="header-data text-center-xs">
                            <div class="hdata">
                                <div class="mcol-left">

                                    <i class="fa fa-money ln-shadow"></i> </div>
                                <div class="mcol-right">

                                    <p><a href="#">650k</a> <em>Sales</em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="hdata">
                                <div class="mcol-left">

                                    <i class="fa fa-users ln-shadow"></i> </div>
                                <div class="mcol-right">

                                    <p><a href="#">7000</a> <em>Users</em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="hdata">
                                <div class="mcol-left">

                                    <i class="fa fa-automobile ln-shadow"></i> </div>
                                <div class="mcol-right">

                                    <p><a href="#">700</a> <em>Products</em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="hdata">
                                <div class="mcol-left">

                                    <i class="icon-th-thumb ln-shadow"></i> </div>
                                <div class="mcol-right">

                                    <p><a href="#">12</a><em>Insurers</em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="hdata">
                                <div class="mcol-left">

                                    <i class="fa fa-bank ln-shadow"></i> </div>
                                <div class="mcol-right">

                                    <p><a href="#">18</a> <em>Banks </em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-box">
                <div class="welcome-msg">
                    <h3 class="page-sub-header2 clearfix no-padding">Hello {{$user->first_name}} {{$user->last_name}} </h3>
                    </div>
                <div id="accordion" class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"> <a href="#collapseB1" data-toggle="collapse"> Summary Page </a> </h4>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseB1">
                            <div class="panel-body">
                              <table class="table table-striped">
                                  <tr>
                                      <td>
                                          Total number of Clients
                                      </td>
                                      <td>
                                          7000
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Total sales so far
                                      </td>
                                      <td>
                                          7000
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>
                                         Total number of products
                                      </td>
                                      <td>
                                          7000
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Total number of Clients
                                      </td>
                                      <td>
                                          7000
                                      </td>
                                  </tr>
                              </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
@stop