@extends('layouts.admin')
@section('page_title','Add new Company')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Add company </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/company/add-company')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Name of company" name="company_name" value="{{old('company_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company Address</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" name="company_address">{{old('company_address')}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contact's person Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Name of contact person" name="contact_person" value="{{old('contact_person')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contact Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" placeholder="jhon.deo@example.com" name="email" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contact Mobile</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="08090678003" name="mobile_no" value="{{old('mobile_no')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company Logo</label>
                            <div class="col-sm-9 mb10">
                                <input id="input-upload-img1" type="file" class="file" data-preview-file-type="text" name="logo">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Phone" class="col-sm-3 control-label">Company Type</label>
                            <div class="col-sm-9">
                               <select name="company_type_id" class="form-control">
                                   <option value="0">Select an option</option>
                                   @foreach ($ct as $c)
                                       <option value="{{$c->company_type_id}}">{{$c->company_type_name}}</option>

                                       @endforeach

                               </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
    @stop