@extends('layouts.admin')
@section('page_title','Manage Your Company')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-home "></i> Manage Available Companies </h2>
                    <div class="table-responsive">
                        @include('partials._message')
                    <table id="addManageTable" class="table table-search"
                           data-filter="#filter" data-filter-text-only="true">
                        <thead>
                        <tr>


                            <th> Company Name</th>
                            <th data-sort-ignore="true"> Type</th>
                            <th data-sort-ignore="true">Offerings</th>
                            <th data-type="numeric"> Contact Name</th>
                            <th> Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($c as $co)
                        <tr>



                            <td  class="add-img-td">{{$co->company_name}}</td>
                            <td  class="ads-details-td">{{$co->company_type()->first()->company_type_name}}</td>
                            <td  class="ads-details-td">{{$co->insurance_schemes()->count()}}</td>
                            <td>{{$co->contact_person}}

                            </td>
                            <td style="width:10%" class="action-td">
                                <div>
                                    <p><a class="btn btn-primary btn-xs"> <i class="fa fa-edit" title="Edit"></i></a>&nbsp;<a class="btn btn-primary btn-xs" href="{{url('admin/company/product')}}/{{$co->company_id}}"> <i class="fa fa-list" title="Offerings"></i></a></p>



                                </div>
                            </td>
                        </tr>

                            @endforeach
                        <tr>
                            <td colspan="5">
                                <?php echo $c->render(); ?>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                        <a class="btn   btn-border btn-post btn-danger" href="{{url('admin/company/add')}}">Add a company</a>
</div>
                </div>

            </div>

        </div>

    </div>
@stop
@section('script')

    <script type="text/javascript">

    </script>

    <script>
        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for(var i=0; i < chkinput.length; i++) {
                if(chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }

    </script>
    @stop