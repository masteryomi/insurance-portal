@extends('layouts.admin')
@section('page_title','Add new Policy')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="fa fa-upload"></i> Insurance Policy Package - {{$c->company_name}}</h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/company/products/add-product')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company_id" value="{{ $c->company_id }}">
                        <div class="form-group">
                            <label for="Phone" class="col-sm-3 control-label">Select Scheme </label>
                            <div class="col-sm-9">
                                <select name="insurance_scheme_id" class="form-control">
                                    <option value="0">Select an option</option>
                                    @foreach ($is as $iss)
                                        <option value="{{$iss->insurance_scheme_id}}">{{$iss->scheme_name}}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Scheme Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="The name of same policy in your company" name="scheme_name" value="{{old('scheme_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="scheme_description" placeholder="give a brief description of the policy">{{old('scheme_description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Policy Logo</label>
                            <div class="col-sm-9 mb10">
                                <input id="input-upload-img1" type="file" class="file" data-preview-file-type="text" name="logo" placeholder="insert a logo. it may be company logo">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Minimum sum insured? </label>
                            <div class="col-sm-9 mb10">
                                <label>
                                    <input type="radio" name="is_minimum_set" id="is_minimum_set" value="Yes">
                                    Yes
                                </label>
                                <label>
                                    <input type="radio" name="is_minimum_set" id="is_minimum_set_no" value="No">
                                    No
                                </label>
                            </div>

                        </div>
                        <div class="form-group amt">
                            <label class="col-sm-3 control-label">Minimum Amount</label>
                            <div class="col-sm-9 mb10">
                                <input type="text" class="form-control" placeholder="the minimum sum insured" name="sum_insured" value="{{old('sum_insured')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rate (%)</label>
                            <div class="col-sm-9 mb10">
                                <input type="text" class="form-control" placeholder="This is the percentage of the sum insured" name="rate" value="{{old('rate')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Features</label>
                            <div class="col-sm-9">
                                <textarea class="textarea form-control" name="features" placeholder="state the features of the scheme">{{old('features')}}</textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script>
        $(document).ready(function(){
            $('.amt').hide();
            $('input:radio[name="is_minimum_set"]').change(function(){
                if($(this).val()=='Yes')
                {
                    $('.amt').show('slow');
                    return;
                }
                $('.amt').hide('slow');

            });

        });
    </script>
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
    <script>
        $('.textarea').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": true, //Button to change color of font
            "size": 'sm' //Button size like sm, xs etc.
        });
    </script>
@stop