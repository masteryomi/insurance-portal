@extends('layouts.admin')
@section('page_title','Company Products')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-diamond "></i> Insurance Packages - {{$c->company_name}}</h2>
                    <div class="table-responsive">

                        @if($c->insurance_schemes()->count() == 0)
                            <div class="alert alert-danger">{{$c->company_name}} has not subscribed to any insurance package. Use the button below to subscribe for an insurance package</div>
                        @else
                        <table id="addManageTable" class="table table-search"
                               data-filter="#filter" data-filter-text-only="true">
                            <thead>
                            <tr>
                                <th> Name</th>
                                <th> Variant</th>
                                <th> Description</th>
                                <th> Sell %</th>
                                <th> Cost %</th>
                                <th> Fixed Price</th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($c->insurance_schemes() as $co)
                                <tr>
                                    <td>{{$co->insurancepolicy()->first()->name}}</td>
                                    <td>{{$co->name}}</td>
                                    <td>{!! wordwrap($co->description,20,"<br>",true) !!}</td>
                                    <td class="">{{number_format($co->percentage,2)}}</td>
                                    <td>{{number_format($co->cost_percentage,2)}}</td>
                                    <td>
                                        @if($co->fixed_price > 0)
                                            &#8358; {{number_format($co->fixed_price,2)}}
                                            @else
                                        No
                                            @endif
                                    </td>
                                    <td>
                                    <a class="btn btn-primary btn-xs" href="{{url('admin/company/product/edit-offering')}}/{{$co->id}}"> <i class="fa fa-edit" title="Edit"></i></a> &nbsp;<a class="btn btn-primary btn-xs" href="{{url('admin/offering/view-details')}}/{{$co->id}}"> <i class="fa fa-search" title="More details"></i></a>
                                        &nbsp;<a class="btn btn-primary btn-xs" href="{{url('admin/offering/view-features')}}/{{$co->id}}"> <i class="fa fa-list" title="View and Add Features"></i></a>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        @endif
                        <a class="btn   btn-border btn-post btn-danger" href="{{url('admin/company/product/add-offering/')}}/{{$c->company_id}}">Add More for  {{$c->company_name}}</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
@stop
@section('script')

    <script type="text/javascript">

    </script>

    <script>
        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for(var i=0; i < chkinput.length; i++) {
                if(chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }

    </script>
@stop