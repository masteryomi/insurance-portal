@extends('layouts.admin')
@section('page_title','Manage Users')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="fa fa-users "></i> Manage Users </h2>
                    <div class="table-responsive">

                        <table id="addManageTable" class="table table-search"
                               >
                            <thead>
                            <tr>


                                <th> Full Name</th>
                                <th>Email Address</th>
                                <th >User Type</th>
                                <th>Status</th>
                                <th> Mobile Number</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $co)
                                <tr>
                                    <td >{{$co->first_name}} {{$co->middle_name}} {{$co->last_name}}</td>
                                    <td  >{{$co->email}}</td>
                                    <td >{{$co->role()->first()->role_name}}</td>
                                    <td  >{{$co->user_status()->first()->status_name}}</td>
                                    <td >{{$co->mobile_number}}

                                    </td>
                                    <td style="width:10%" class="action-td">
                                        <div>
                                            <p><a class="btn btn-primary btn-xs"> <i class="fa fa-edit"></i> Edit </a></p>

                                            <p><a class="btn btn-primary btn-xs" href="{{url('admin/product/packages')}}/{{$co->user_id}}"> <i class="fa fa-list"></i> Packages </a></p>

                                            <p><a class="btn btn-danger btn-xs"> <i class=" fa fa-trash"></i> Delete </a></p>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <a class="btn   btn-border btn-post btn-danger" href="{{url('admin/products/add')}}">Add a new policy</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
@stop
@section('script')

    <script type="text/javascript">

    </script>

    <script>
        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });
        function checkAll(bx) {
            var chkinput = document.getElementsByTagName('input');
            for(var i=0; i < chkinput.length; i++) {
                if(chkinput[i].type == 'checkbox') {
                    chkinput[i].checked = bx.checked;
                }
            }
        }

    </script>
@stop