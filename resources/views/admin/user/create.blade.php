@extends('layouts.admin')
@section('page_title','Add new User')
@section('content')
    <div class="container">
        <div class="row">
            @include('partials._adminlinks')

            <div class="col-sm-9 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Add User </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('admin/company/add-company')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="first name" name="first_name" value="{{old('first_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="last name" name="last_name" value="{{old('last_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Middle Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="middle name" name="middle_name" value="{{old('middle_name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Email Address</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" placeholder="jhon.deo@example.com" name="email" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control"  name="password " >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="08090678003" name="mobile_no" value="{{old('mobile_no')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Phone" class="col-sm-3 control-label">Select Company </label>
                            <div class="col-sm-9">
                                <select name="company_id" class="form-control">
                                    <option value="0">Select an option</option>
                                    @foreach ($company as $c)
                                        <option value="{{$c->company_id}}">{{$c->company_name}}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Phone" class="col-sm-3 control-label">User Type</label>
                            <div class="col-sm-9">
                                <select name="company_type_id" class="form-control">
                                    <option value="0">Select an option</option>
                                    @foreach ($role as $c)
                                        <option value="{{$c->role_id}}">{{$c->role_name}}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
@stop
@section('script')
    <script src="{{{asset('assets/js/fileinput.min.js')}}}" type="text/javascript"></script>
    <script>
        // initialize with defaults
        $("#input-upload-img1").fileinput();


    </script>
@stop