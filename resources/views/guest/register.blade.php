@extends('layouts.guest')
@section('page_title','Welcome')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-user-add"></i> Create New Account </h2>

                    @include('partials._message')

                    <form class="form-horizontal" role="form" method="post" action="{{url('guest/create-account')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="first name" name="first_name" value="{{old('first_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="last name" name="last_name" value="{{old('last_name')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Middle Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="middle name" name="middle_name" value="{{old('middle_name')}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Email Address</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" placeholder="jhon.deo@example.com" name="email" value="{{old('email')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control"  name="password" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Re-type Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control"  name="password1" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="08099009900" name="mobile_no" value="{{old('mobile_no')}}">
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">Add</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>


            <div class="col-md-4 reg-sidebar">
                <div class="reg-sidebar-inner text-center">
                    <div class="promo-text-box"> <i class=" icon-info-circled
                     fa fa-4x icon-color-1"></i>
                        <h3><strong>Get Lastest Information</strong></h3>
                        <p> We have up to date information about your favourate insurance companies and can subscribe you to their offers </p>
                    </div>
                    <div class="promo-text-box"> <i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
                        <h3><strong>Be A Merchant</strong></h3>
                        <p> You can simply activate the merchant module by signing up, activating your account and updating your records</p>
                    </div>
                    <div class="promo-text-box"> <i class="  icon-search fa fa-4x icon-color-3"></i>
                        <h3><strong>Search various Listings</strong></h3>
                        <p> Youc an search for various policies and get instant policy number.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop