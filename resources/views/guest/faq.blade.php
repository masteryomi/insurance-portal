@extends('layouts.guest')
@section('page_title','Contact Us')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 page-content">

                <div class="inner-box">
                    <h2 class="title-2"><i class="glyphicon glyphicon-question-sign"></i> Frequently Asked Questions </h2>


                </div>

            </div>


            <div class="col-md-4 reg-sidebar">
                <div class="reg-sidebar-inner text-center">
                    <div class="promo-text-box"> <i class=" icon-info-circled
                     fa fa-4x icon-color-1"></i>
                        <h3><strong>Get Lastest Information</strong></h3>
                        <p> We have up to date information about your favourate insurance companies and can subscribe you to their offers </p>
                    </div>
                    <div class="promo-text-box"> <i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
                        <h3><strong>Be A Merchant</strong></h3>
                        <p> You can simply activate the merchant module by signing up, activating your account and updating your records</p>
                    </div>
                    <div class="promo-text-box"> <i class="  icon-search fa fa-4x icon-color-3"></i>
                        <h3><strong>Search various Listings</strong></h3>
                        <p> Youc an search for various policies and get instant policy number.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop