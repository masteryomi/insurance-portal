@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
    @include('partials._search')
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('partials._sidecategory')

                <div class="col-sm-9 page-content col-thin-left">
                    <div class="inner-box category-content">
                        <h2 class="title-2"><i class="fa fa-umbrella"></i> Insurance Company - {{$company1->company_name}} </h2>
                        @if($company1->policies()->get()->count() == 0)
                            <div class="alert alert-warning pgray  alert-lg" role="alert">
                                <h2 class="no-margin no-padding"><i class="fa fa-warning"></i> Sorry! No product exist for this insurer at the moment</h2>
                                <p>You can always check back or you can browse through other insurance companies </p>
                            </div>
                        @endif
                        @foreach($company1->policies()->get() as $policy)
                            <div class="col-md-6 col-sm-6">
                                <div class="panel sidebar-panel panel-contact-seller borders">
                                    <div class="panel-heading" title="{{$policy->name}}">{{str_limit($policy->name,20)}}</div>
                                    <div class="panel-content user-info">
                                        <div class="panel-body ">
                                            <div class="seller-info">
                                                <table class="table table-action ">
                                                    <tr>
                                                        <td><b>Variant</b></td>
                                                        <td><p class="text-danger">{{$policy->insurancepolicy()->first()->name}}</p> </td>
                                                    </tr>

                                                    @if($policy->fixed_price > 0)
                                                        <tr>
                                                            <td><b>Fixed Price</b></td>
                                                            <td><p class="text-danger">&#8358; {{number_format($policy->fixed_price,2)}}</p> </td>

                                                        @else
                                                        <tr>
                                                            <td><b>Percentage</b></td>
                                                            <td><p class="text-danger">{{number_format($policy->percentage,2)}} %</p> </td>
                                                        </tr>
                                                        @endif
                                                                                                       <tr>
                                                        <td colspan="2">
                                                            <p>{{str_limit($policy->description,80)}}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <a  href="{{url("guest/products/")}}/{{$policy->id}}" class="btn  btn-success btn-block"><i class=" icon-money"></i> Buy Policy </a>
                                                        </td>
                                                    </tr>
                                                    @if($policy->getSimilarOfferings()->count() > 0)
                                                        @if($policy->getSimilarOfferings()->count() == 1)

                                                    <tr>
                                                        <td colspan="2">
                                                            {{$policy->getSimilarOfferings()->count()}} similar policy
                                                            <ul>
                                                                @foreach($policy->getSimilarOfferings()->get() as $similar)
                                                                    <li> {{$similar->company_name}} (<b>{{$similar->name}}</b>) - {{$similar->percentage}} %</li>
                                                                @endforeach
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                            @else
                                                            <tr>
                                                                <td colspan="2">
                                                                    {{$policy->getSimilarOfferings()->count()}} similar policies <br>
                                                                    <ul>
                                                                        @foreach($policy->getSimilarOfferings()->get() as $similar)
                                                                        <li> {{$similar->company_name}} (<b>{{$similar->name}}</b>) - {{$similar->percentage}} %</li>
                                                                        @endforeach
                                                                    </ul>

                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @else
                                                        <tr>
                                                            <td colspan="2">
                                                                No similar policy exist
                                                            </td>
                                                        </tr>
                                                        @endif
                                                </table>

                                            </div>

                                </div>
                            </div>
                    </div>



                </div>
                @endforeach
            </div>


        </div>

    </div>
    </div>
    </div>


@stop
