@extends('layouts.user')
@section('page_title','Welcome')
@section('content')


    <div class="main-container">
        <div class="container">
            <ol class="breadcrumb pull-left">
                <li><a href="{{url("/")}}"><i class="icon-home fa"></i></a></li>
                <li><a href="{{url("guest/insurer/")}}/{{$product->company()->first()->company_id}}">{{$product->company()->first()->company_name}}</a></li>
                <li><a href="{{url("guest/product/")}}/{{$product->insurancepolicy()->first()->id}}">{{$product->insurancepolicy()->first()->name}}</a></li>
                <li class="active">{{$product->name}}</li>
            </ol>
            <div class="pull-right backtolist"><a href="{{url("guest/product/")}}/{{$product->insurancepolicy()->first()->id}}"> <i class="fa fa-angle-double-left"></i> Back to Results</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2> {{$product->name}} <small class="label label-default adlistingtype">{{$product->insurancepolicy()->first()->name}}</small> </h2>
                        {{--<span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Today 1:21 pm </span> - <span class="category">{{$product->insurancepolicy()->first()->name}} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> Nigeria </span> </span>--}}
                        <br>
                        <div class="ads-image">
                            @if($product->fixed_price > 0)
                                <h1 class="pricetag"> &#8358; {{number_format($product->fixed_price,2)}}</h1>
                                @else
                                <h1 class="pricetag"> {{number_format($product->percentage,2)}}% of sum insured</h1>
                                @endif

                                <br><br>

                            {{--<ul class="bxslider">--}}
                                {{--<li><center>--}}
                                        {{--<img src="{{asset('images/umbrella.jpg')}}" alt="img" class="img-responsive "  />--}}
                                    {{--</center></li>--}}

                            {{--</ul>--}}
                            {{--<div id="bx-pager"> <a class="thumb-item-link" data-slide-index="0" href="#"><img src="{{asset('images/umbrella.jpg')}}" alt="img" class="img-thumbnail"/></a> s</div>--}}
                        {{----}}
                        </div>

                        <div class="Ads-Details">
                            <h5 class="list-title"><strong>Policy Details</strong></h5>
                            <div class="row">
                                <div class="ads-details-info col-md-8">
                                    <p>{{$product->description}} </p>
                                    <h4>Features</h4>
                                    <ul class="list-circle">
                                        @foreach($product->features()->get() as $feature)
                                            <li>{{$feature->description}}</li>
                                            @endforeach


                                    </ul>

                                </div>
                                <div class="col-md-4">
                                    <aside class="panel panel-body panel-details">
                                        <ul>
                                            <li>
                                                <p class=" no-margin "><strong>Price:</strong>
                                                @if($product->fixed_price > 0)
                                                     &#8358; {{number_format($product->fixed_price,2)}}
                                                @else
                                                     {{number_format($product->percentage,2)}}% of sum insured
                                                @endif
                                                  </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Type:</strong> {{$product->insurancepolicy()->first()->name}}</p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Location:</strong> Nigeria </p>
                                            </li>
                                             <li>
                                                <p class="no-margin"><strong>Brand:</strong> {{$product->company()->first()->company_name}}</p>
                                            </li>
                                        </ul>
                                    </aside>
                                    <div class="ads-action">
                                        <ul class="list-border">
                                            <li><a href="{{url("guest/insurer")}}/{{$product->company()->first()->company_id}}"> <i class=" fa fa-user"></i> More from {{$product->company()->first()->company_name}} </a> </li>
                                            <li><a href="{{url("guest/product")}}/{{$product->insurancepolicy()->first()->id}}"> <i class=" fa fa-heart"></i> More {{$product->insurancepolicy()->first()->name}} products </a> </li>
                                           </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-footer text-left"> <a class="btn  btn-success" data-toggle="modal" href="{{url("user/product/buy")}}/{{$product->id}}"><i class=" icon-mail-2"></i> Buy Policy </a> <a class="btn  btn-info"><i class=" icon-phone-1"></i>{{config("insurance.mobileno")}} </a> </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Buy Policy</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{config('insurance.default_seller')}} <small>Agent</small></h3>
                                        <p>Location: <strong>Nigeria</strong></p>
                                        <p> Sold: <strong>{{$product->sold()->get()->count()}}</strong></p>
                                    </div>
                                    <div class="user-ads-action"> <a href="{{url("user/product/buy")}}/{{$product->id}}" data-toggle="modal" class="btn   btn-success btn-block"><i class=" icon-money"></i> Place Order</a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel sidebar-panel">
                            <div class="panel-heading">Similar Products</div>
                            <div class="panel-content">
                                <div class="panel-body text-left">
                                    <ul class="list-check">
                                        @foreach($product->getSimilarOfferings()->get() as $similar)
                                            <li><a href="{{url("guest/products/")}}/{{$similar->id}}"> {{str_limit($similar->company_name,12)}} (<b>{{str_limit($similar->name,12)}}</b>) - {{$similar->percentage}} %</a></li>
                                        @endforeach


                                    </ul>
                                    <p><a class="pull-right" href="#"> Know more <i class="fa fa-angle-double-right"></i> </a></p>
                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

@stop