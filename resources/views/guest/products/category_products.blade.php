@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
@include('partials._search')
    <div class="main-container">
        <div class="container">
            <div class="row">
@include('partials._sidecategory')

                <div class="col-sm-9 page-content col-thin-left">
                    <div class="inner-box category-content">
                        <h2 class="title-2"><i class="icon-folder-close"></i> Products - {{$category1->name}} </h2>

                        @foreach($category1->policies()->get() as $policy)
                            <div class="col-md-4 col-sm-4">
                                <div class="panel sidebar-panel panel-contact-seller borders">
                                    <div class="panel-heading" title="{{$policy->name}}">{{str_limit($policy->name,20)}}</div>
                                    <div class="panel-content user-info">
                                        <div class="panel-body text-center">
                                            <div class="seller-info">

                                                <p>{{$policy->description}}</p>

                                            </div>
                                            <div class="user-ads-action">
                                                @if($policy->offerings()->count() > 0)
                                                    @if($policy->offerings()->count() ==1)
                                                    <a href="#" data-toggle="modal" class="btn   btn-info btn-block"><i class=" icon-mail-2"></i> View {{$policy->offerings()->count()}} policy </a>
                                                        <a class="btn  btn-success btn-block"><i class=" icon-money"></i> Buy Policy </a> </div>
                                                        @elseif ($policy->offerings()->count()  > 1)
                                                        <a href="" data-toggle="modal" class="btn   btn-danger btn-block"><i class=" icon-mail-2"></i> Compare {{$policy->offerings()->count()}} policies </a>
                                                        <a class="btn  btn-default btn-block"><i class=" icon-money"></i> Buy Policy </a> </div>
                                                    @endif
                                                @else
                                                    <a href="#"  class="btn   btn-default btn-block "><i class=" icon-info-circled"></i> No Policy Available </a>
                                                    <a class="btn  btn-default btn-block"><i class=" icon-money"></i> Buy Policy </a> </div>
                                                @endif

                                        </div>
                                    </div>
                                </div>



                            </div>
                            @endforeach
                    </div>


                </div>

            </div>
        </div>
    </div>


@stop
