@extends('layouts.user')
@section('page_title','Welcome')
@section('content')
    <div class="intro">
        <div class="dtable hw100">
            <div class="dtable-cell hw100">
                <div class="container text-center">
                    <h1 class="intro-title animated fadeInDown"> Search & Buy Insurance Policies </h1>
                    <p class="sub animateme fittext3 animated fadeIn"> You can find insurance policies, compare prices and features before paying online </p>
                    <div class="row search-row animated fadeInUp">

                        <div class="col-lg-8 col-sm-8 search-col relative"> <i class="icon-docs icon-append"></i>
                            <input type="text" name="ads" class="form-control has-icon" placeholder="I'm looking for a ..." value="">
                        </div>
                        <div class="col-lg-4 col-sm-4 search-col">
                            <button class="btn btn-primary btn-search btn-block"><i class="icon-search"></i><strong>Find</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner-box category-content">
                        <h2 class="title-2">Search for Insurance products </h2>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 ">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/index")}}"><i class="fa fa-list ln-shadow"></i> Products <span class="count">{{$category->count()}}</span> </a>
                                        <span data-target=".cat-id-1" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-1">
                                        @foreach ($category as $cat)
                                        <li> <a href="{{url("guest/category/")}}/{{$cat->id}}">{{$cat->name}} ({{$cat->policies()->count()}})</a> </li>

                                     @endforeach
                                    </ul>
                                </div>

                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/1")}}"><i class="icon-ambulance ln-shadow"></i> Life Products<span class="count">&nbsp;</span></a>
                                        <span data-target=".cat-id-3" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-3">
                                        @foreach($category as $cat)
                                            @if($cat->id == 1)
                                             @foreach($cat->policies()->get() as $cats)
                                                <li> <a href="{{url("guest/product/")}}/{{$cats->id}}">{{$cats->name}} ({{$cats->offerings()->count()}})</a></li>
                                             @endforeach
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/insurer/index")}}"><i class="fa fa-umbrella ln-shadow"></i> Insurers <span class="count">{{$company->count()}}</span></a>
                                        <span data-target=".cat-id-4" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-4">
                                        @foreach($company as $com)
                                        <li> <a href="{{url("guest/insurer/")}}/{{$com->company_id}}">{{$com->company_name}} ({{$com->policies()->count()}})</a> </li>
                                       @endforeach

                                    </ul>
                                </div>


                            </div>
                            <div class="col-md-4 col-sm-4   last-column">
                                <div class="cat-list">
                                    <h3 class="cat-title"><a href="{{url("guest/category/2")}}"><i class="icon-ambulance ln-shadow"></i> Non-Life Products<span class="count">&nbsp;</span></a>
                                        <span data-target=".cat-id-3" data-toggle="collapse" class="btn-cat-collapsed collapsed"> <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-3">
                                        @foreach($category as $cat)
                                            @if($cat->id == 2)
                                                @foreach($cat->policies()->get() as $cats)
                                                    <li> <a href="{{url("guest/product/")}}/{{$cats->id}}">{{$cats->name}} ({{$cats->offerings()->count()}})</a></li>
                                                @endforeach
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="inner-box relative">
                        <h2 class="title-2">Featured Listings
                            <a id="nextItem" class="link  pull-right carousel-nav"> <i class="icon-right-open-big"></i></a>
                            <a id="prevItem" class="link pull-right carousel-nav"> <i class="icon-left-open-big"></i> </a>
                        </h2>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="no-margin item-carousel owl-carousel owl-theme">
                                    <div class="item"> <a href="ads-details.html">
<span class="item-carousel-thumb">
<img class="img-responsive" src="images/item/tp/Image00011.jpg" alt="img">
</span>
                                            <span class="item-name"> Lorem ipsum dolor sit amet </span>
                                            <span class="price"> $ 260 </span>
                                        </a>
                                    </div>
                                    <div class="item"> <a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/tp/Image00006.jpg" alt="img"> </span>
                                            <span class="item-name"> consectetuer adipiscing elit </span>
                                            <span class="price"> $ 240 </span></a> </div>
                                    <div class="item"><a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/tp/Image00022.jpg" alt="img"> </span> <span class="item-name"> sed diam nonummy </span> <span class="price"> $ 140</span></a></div>
                                    <div class="item"><a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/tp/Image00013.jpg" alt="img"> </span><span class="item-name"> feugiat nulla facilisis </span> <span class="price"> $ 140 </span></a></div>
                                    <div class="item"><a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/FreeGreatPicture.com-46404-google-drops-nexus-4-by-100-offers-15-day-price-protection-refund.jpg" alt="img"> </span> <span class="item-name"> praesent luptatum zzril </span> <span class="price"> $ 220 </span></a></div>
                                    <div class="item"><a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/FreeGreatPicture.com-46405-google-drops-price-of-nexus-4-smartphone.jpg" alt="img"> </span> <span class="item-name"> delenit augue duis dolore </span> <span class="price"> $ 120 </span></a></div>
                                    <div class="item"><a href="ads-details.html">
                                            <span class="item-carousel-thumb"> <img class="item-img" src="images/item/FreeGreatPicture.com-46407-nexus-4-starts-at-199.jpg" alt="img"> </span> <span class="item-name"> te feugait nulla facilisi </span> <span class="price"> $ 251 </span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3 page-sidebar col-thin-left">
                    <aside>
                        <div class="inner-box no-padding">
                            <div class="inner-box-content"> <a href="#"><img class="img-responsive" src="images/site/app.jpg" alt="tv"></a> </div>
                        </div>
                        <div class="inner-box">
                            <h2 class="title-2">Popular Categories </h2>
                            <div class="inner-box-content">
                                <ul class="cat-list arrow">
                                    @foreach ($category as $cat)
                                        <li> <a href="{{url("guest/category/")}}/{{$cat->id}}">{{$cat->name}} ({{$cat->policies()->count()}})</a> </li>

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="inner-box no-padding"> <img class="img-responsive" src="images/add2.jpg" alt=""> </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>



@stop
