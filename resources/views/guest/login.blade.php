@extends('layouts.guest')
@section('page_title','Welcome')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">
                <div class="panel panel-default">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">

                            <span class="logo-icon"><i class="icon icon-login"></i> </span> <span>Member Login</span></h2>
                    </div>
                    <div class="panel-body">
                        @include('partials._message')
                        <form role="form" action="{{url('guest/auth-login')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="sender-email" class="control-label">Email Address:</label>
                                <div class="input-icon"> <i class="icon-user fa"></i>
                                    <input id="sender-email" type="email" placeholder="email address" class="form-control email" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user-pass" class="control-label">Password:</label>
                                <div class="input-icon"> <i class="icon-lock fa"></i>
                                    <input type="password" class="form-control" placeholder="Password" id="user-pass" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary  btn-block">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <label class=" pull-left">
                            <p> Don't have an account? <a href="{{url('guest/sign-up')}}"><strong>Sign Up !</strong> </a> </p> </label>
                        <p class="text-center pull-right"> <a href="{{url('guest/password/email')}}"> Lost your password? </a> </p>
                        <div style=" clear:both"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop