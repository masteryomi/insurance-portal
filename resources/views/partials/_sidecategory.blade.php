<div class="col-sm-3 page-sidebar">
    <aside>
        <div class="inner-box">
            <div class="categories-list  list-filter">
                <h5 class="list-title"><strong><a href="#">All Categories</a></strong></h5>
                <ul class=" list-unstyled">

                    @foreach ($category as $cat)
                        <li> <a href="{{url("guest/category/")}}/{{$cat->id}}">{{$cat->name}} <span class="count">({{$cat->policies()->count()}})</span></a> </li>

                    @endforeach
                </ul>
            </div>

            <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Insurers</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                    @foreach($company as $com)
                        <li> <a href="{{url("guest/insurer/")}}/{{$com->company_id}}">{{$com->company_name}} ({{$com->policies()->count()}})</a> </li>
                    @endforeach
                </ul>
            </div>







            <div style="clear:both"></div>
        </div>

    </aside>
</div>