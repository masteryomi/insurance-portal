<div class="col-sm-3 page-sidebar">
    <aside>
        <div class="inner-box">
            <div class="user-panel-sidebar">
                <div class="collapse-box">
                    <h5 class="collapse-title no-border"> My Links <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="MyClassified">
                        <ul class="acc-list">
                            <li><a  href="{{url('user/dashboard')}}"><i class="icon-home"></i> Dashboard </a></li>
                        </ul>
                    </div>
                </div>

                <div class="collapse-box">
                    <h5 class="collapse-title"> Products <a href="#MyAds" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="MyAds">
                        <ul class="acc-list">
                            <li><a href="{{url('user/product/history')}}"><i class="fa fa-history"></i> History  </a></li>
                            <li><a href="{{url('user/')}}"><i class="fa fa-shopping-cart"></i> Products </a></li>
                            <li><a href="{{url('user/payments')}}"><i class="fa fa-money"></i> Insurance Payments </a></li>

                        </ul>
                    </div>
                </div>

                <div class="collapse-box">
                    <h5 class="collapse-title"> Profile <a href="#TerminateAccount" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="TerminateAccount">
                        <ul class="acc-list">

                            <li><a href="{{url('user/profile')}}"><i class="fa fa-user "></i> Profile</a></li>
                            <li><a href="{{url('user/signout')}}"><i class="fa fa-sign-out "></i>Logout</a></li>
                        </ul>
                    </div>
                </div>



            </div>
        </div>

    </aside>
</div>