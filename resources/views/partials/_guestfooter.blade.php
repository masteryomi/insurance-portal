<div class="footer" id="footer">
    <div class="container">
        <ul class=" pull-left navbar-link footer-nav">
            <li><a href="{{url('/')}}"> Home </a>
                <a href="{{url('about-us')}}"> About us </a>
                <a href="{{url('/find-an-agent')}}"> Find an Agent </a>
                <a href="{{url('/contact-us')}}"> Contact us </a>
                <a href="{{url('/faq')}}"> FAQ </a>
        </ul>
        <ul class=" pull-right navbar-link footer-nav">
            <li> &copy; 2015 {{ config('insurance.app_name') }} </li>
        </ul>
    </div>
</div>

</div>
<script src="{{{ asset('assets/js/jquery.min.js')}}}">
</script>
<script src="{{{ asset('assets/js/wysihtml5-0.3.0.js')}}}"></script>
<script src="{{{ asset('assets/bootstrap/js/bootstrap.min.js')}}}"></script>
<script src="{{{ asset('assets/js/bootstrap3-wysihtml5.js')}}}"></script>

<script src="{{{ asset('assets/js/owl.carousel.min.js')}}}"></script>

<script src="{{{ asset('assets/js/form-validation.js')}}}"></script>

<script src="{{{ asset('assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')}}}"></script>
<script src="{{{ asset('assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')}}}"></script>


@yield('script')
</body>

</html>
