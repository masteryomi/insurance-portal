<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png')}}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('page_title')</title>
    <link href="{{{ asset('assets/bootstrap/css/bootstrap.min.css') }}}" rel="stylesheet" type="text/css">
    <link href="{{{ asset('assets/css/bootstrap-wysihtml5.css')}}}" rel="stylesheet">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png')}}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png')}}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png')}}}">
    <link rel="apple-touch-icon-precomposed" href="{{{ asset('ico/apple-touch-icon-57-precomposed.html')}}}">


    <link href="{{{ asset('assets/css/style.css')}}}" rel="stylesheet">
    <link href="{{{ asset('assets/css/fileinput.min.css')}}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{{ asset('assets/css/owl.carousel.css')}}}" rel="stylesheet">
    <link href="{{{ asset('assets/css/owl.theme.css')}}}" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="{{{ asset('assets/js/pace.min.js')}}}"></script>
</head>
<body>
<div id="wrapper">