
<div class="search-row-wrapper">
    <div class="container ">
        <form action="#" method="GET">
            <div class="col-sm-3">
                <input class="form-control keyword" type="text" placeholder="e.g. Looking for ?">
            </div>
            <div class="col-sm-3">
                <select class="form-control selecter" name="category" id="search-category">
                    <option selected="selected" value="">All Products</option>

                    @foreach($category as $cat)
                        @if($cat->id == 1)
                            <option value="{{$cat->name}}" style="background-color:#E9E9E9;font-weight:bold;" disabled="disabled"> - {{$cat->name}} - </option>
                            @foreach($cat->policies()->get() as $cats)
                                <option value="{{$cats->id}}"> {{$cats->name}} ({{$cats->offerings()->count()}}) </option>

                            @endforeach
                        @endif
                            @if($cat->id == 2)
                                <option value="{{$cat->name}}" style="background-color:#E9E9E9;font-weight:bold;" disabled="disabled"> - {{$cat->name}} - </option>
                                @foreach($cat->policies()->get() as $cats)
                                    <option value="{{$cats->id}}"> {{$cats->name}} ({{$cats->offerings()->count()}}) </option>

                                @endforeach
                            @endif
                    @endforeach

                </select>
            </div>
            <div class="col-sm-3">
                <select class="form-control selecter" name="location" id="id-location">
                    <option selected="selected" value="">Nigeria</option>

                </select>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-block btn-primary  "> <i class="fa fa-search"></i> </button>
            </div>
        </form>
    </div>
</div>
