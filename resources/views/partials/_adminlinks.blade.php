<div class="col-sm-3 page-sidebar">
    <aside>
        <div class="inner-box">
            <div class="user-panel-sidebar">
                <div class="collapse-box">
                    <h5 class="collapse-title no-border"> My Classified <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="MyClassified">
                        <ul class="acc-list">
                            <li><a class="active" href="{{url('admin/dashboard')}}"><i class="icon-home"></i> Dashboard </a></li>
                        </ul>
                    </div>
                </div>

                <div class="collapse-box">
                    <h5 class="collapse-title"> Configurations <a href="#MyAds" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="MyAds">
                        <ul class="acc-list">
                            <li><a href="{{url('admin/users/all')}}"><i class="icon-users"></i> All Users  </a></li>
                             <li><a href="{{url('admin/users/create')}}"><i class="icon-user-add"></i> Create User </a></li>
                            <li><a href="{{url('admin/company/manage')}}"><i class="icon-home"></i> Manage Company </a></li>
                            </ul>
                    </div>
                </div>

                <div class="collapse-box">
                    <h5 class="collapse-title"> Products <a href="#TerminateAccount" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                    <div class="panel-collapse collapse in" id="TerminateAccount">
                        <ul class="acc-list">
                            {{--<li><a href="{{url('admin/products/all')}}"><i class="fa fa-list "></i> Manage Products</a></li>--}}
                            <li><a href="{{url('admin/payments/manage')}}"><i class="fa fa-money "></i> Payment History</a></li>
                        </ul>
                    </div>
                </div>



            </div>
        </div>

    </aside>
</div>