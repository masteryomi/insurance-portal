<!DOCTYPE html>
@include('partials._header')
<div class="header">
    <nav class="navbar navbar-site navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="index-2.html" class="navbar-brand logo logo-title">

                    <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> <span>{{config('insurance.app_name')}} </span> </a> </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('admin/logout')}}">Signout <i class="glyphicon glyphicon-off"></i> </a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{$user->first_name}} {{ $user->last_name}}</span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul class="dropdown-menu user-menu">
                            <li class="active"><a href="{{url('admin/dashboard')}}"><i class="icon-home"></i> Personal Home </a></li>
                            <li><a href="{{url('admin/my-profile')}}"><i class="icon-th-thumb"></i> My Profile </a></li>
                            <li><a href="{{url('admin/change-password')}}"><i class="icon-lock"></i> Change Password </a></li>
                            <li><a href="{{url('admin/payments/history')}}"><i class=" icon-money "></i> Payment history </a></li>
                        </ul>
                    </li>

                </ul>
            </div>

        </div>

    </nav>
</div>
<div class="main-container">
    @yield('content')
</div>

@include('partials._footer')