<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>

        Welcome to {{config('insurance.app_name')}}

    </title>
    <style>
        .innerPg
        {
            min-height:500px;
            font-size:14px;
            font-weight:lighter !important;
            line-height:20px;
            margin-top:40px;
            margin-bottom:20px;
        }
        .innerPg form ul
        {
            margin:0;
            padding:0;

        }
        .title-2 {
            border-bottom: 1px solid #e6e6e6;
            margin-bottom: 20px;
        }
        .inner-box.category-content {
            padding-bottom: 0;
        }
        .page-content .inner-box {
            overflow: auto;
            padding: 15px;
        }
        .inner-box {
            background: #cccccc none repeat scroll 0 0;
            border-radius: 3px;
            box-shadow: 0 1px 1px rgba(180, 180, 180, 0.5);
            margin-bottom: 30px;
            padding: 20px 15px;
        }
    </style>
</head>

<body>
<div class="inner-box category-content">

    @yield('content')
</div>

</body>
</html>