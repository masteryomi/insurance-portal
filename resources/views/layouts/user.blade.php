<!DOCTYPE html>
@include('partials._guestheader')
@if (!Auth::check())
<div class="header">
    <nav class="navbar   navbar-site navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="{{url('guest/')}}" class="navbar-brand logo logo-title">

                    <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> <span>{{config('insurance.app_name')}} </span> </a> </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('guest/login')}}"><i class="icon-th-thumb"></i> Login </a></li>
                    <li><a href="{{url('guest/sign-up')}}"><i class="icon-lock"></i> Sign Up </a></li>
                    <li class="postadd"><a class="btn btn-block btn-border btn-post btn-danger" href="{{url('guest/sign-up')}}">Create Account m</a></li>
                </ul>
            </div>

        </div>

    </nav>
</div>
@else
    <div class="header">
        <nav class="navbar   navbar-site navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a href="{{url('guest/')}}" class="navbar-brand logo logo-title">

                        <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> <span>{{config('insurance.app_name')}} </span> </a> </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{url('user/dashboard')}}"><i class="fa fa-history"></i> Dashboard </a></li>
                        <li><a href="{{url('user/signout')}}"><i class="icon-logout"></i> Logout</a></li>

                    </ul>
                </div>

            </div>

        </nav>
    </div>
    @endif

    @yield('content')
@include('partials._guestfooter')