<!DOCTYPE html>
@include('partials._guestheader')
<div class="header">
    <nav class="navbar   navbar-site navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="{{url("/")}}" class="navbar-brand logo logo-title">

                    <span class="logo-icon"><i class="icon icon-search ln-shadow-logo shape-0"></i>
                    </span> {{config('insurance.app_name')}} </a> </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('guest/login')}}">Login</a></li>
                    <li><a href="{{url('guest/sign-up')}}">Signup</a></li>

                </ul>
            </div>

        </div>

    </nav>
</div>

<div class="main-container">

   @yield('content')
</div>

@include('partials._footer')